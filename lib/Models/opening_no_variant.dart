import 'model.dart';

class InvariantOpening implements Model {
  static String table = 'invariant_openings';

  int id;
  String name;
  String movements;
  String movementsTextual;
  String attachedVariants;
  String subName;
  String side;

  InvariantOpening({
    required this.side,
    required this.id,
    required this.name,
    required this.subName,
    required this.movements,
    required this.movementsTextual,
    required this.attachedVariants,
  });

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'name': name,
      'subName': subName,
      'movements': movements,
      'movementsTextual': movementsTextual,
      'attachedVariants': attachedVariants,
      'side': side
    };
    return map;
  }

  static InvariantOpening fromMap(Map<String, dynamic> map) {
    return InvariantOpening(
        id: map['id'],
        name: map['name'],
        subName: map["subName"],
        movements: map['movements'],
        movementsTextual: map['movementsTextual'],
        attachedVariants: map['attachedVariants'],
        side: map['side']);
  }
}
