import 'dart:convert';

import 'model.dart';


class Commentary implements Model {

  static String table = 'commentaries';

  int id;
  int openingID;
  int attachToIndex;
  String side; //  b or w
  String commentaryText;

  Commentary({
    required this.id,
    required this.openingID,
    required this.attachToIndex,
    required this.side,
    required this.commentaryText,
  });

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'openingID': openingID,
      'attachToIndex' : attachToIndex,
      'side': side,
      'commentaryText': commentaryText,
    };
    return map;
  }

  static Commentary fromMap(Map<String, dynamic> map) {
    return Commentary(
        id: map['id'],
        openingID: map['openingID'],
        attachToIndex: map["attachToIndex"],
        side: map['side'],
        commentaryText: map['commentaryText'],
    );
  }
}