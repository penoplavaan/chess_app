import 'dart:convert';

import 'model.dart';


class VariantCommentary implements Model {

  static String table = 'variant_commentaries';

  int id;
  int openingID;
  int attachToIndex;
  String side; //  b or w
  String commentaryText;

  VariantCommentary({
    required this.id,
    required this.openingID,
    required this.attachToIndex,
    required this.side,
    required this.commentaryText,
  });

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'openingID': openingID,
      'attachToIndex' : attachToIndex,
      'side': side,
      'commentaryText': commentaryText,
    };
    return map;
  }

  static VariantCommentary fromMap(Map<String, dynamic> map) {
    return VariantCommentary(
        id: map['id'],
        openingID: map['openingID'],
        attachToIndex: map["attachToIndex"],
        side: map['side'],
        commentaryText: map['commentaryText'],
    );
  }
}