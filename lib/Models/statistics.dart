

import 'model.dart';


class Statistics implements Model {

  static String table = 'statistics';

  int id;
  int openingId;
  int total_play;
  int mistakes ;
  int wins ;
  int loses;


  Statistics({
    required this.id,
    required this.openingId,
    required this.total_play,
    required this.mistakes,
    required this.wins,
    required this.loses,
  });

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'id': id,
      'openingId': openingId,
      'total_play': total_play,
      'mistakes': mistakes,
      'wins': wins,
      'loses': loses,
    };
    return map;
  }

  static Statistics fromMap(Map<String, dynamic> map) {
    return Statistics(
      id: map['id'],
      openingId: map['openingId'],
      total_play: map['total_play'],
      mistakes: map['mistakes'],
      wins: map['wins'],
      loses: map['loses'],
    );
  }
}