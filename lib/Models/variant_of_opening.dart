

import 'model.dart';


class OpeningVariant implements Model {

  static String table = 'opening_variants';

  int id;
  int attachToIndex;
  String movements;
  String movementsTextual;
  int importance;

  OpeningVariant({ required this.id, required this.attachToIndex, required this.movements, required this.movementsTextual,required this.importance });

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'attachToIndex': attachToIndex,
      'movements': movements,
      'movementsTextual': movementsTextual,
      'importance' : importance
    };
    return map;
  }

  static OpeningVariant fromMap(Map<String, dynamic> map) {
    return OpeningVariant(
        id: map['id'],
        attachToIndex: map['attachToIndex'],
        movements: map['movements'],
        movementsTextual: map['movementsTextual'],
        importance: map['importance'],
    );
  }
}