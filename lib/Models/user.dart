

import 'model.dart';


class User implements Model {

  static String table = 'user';

  int id;
  int first_time_opened;
  int? externalId;

  User({
    required this.id,
    required this.first_time_opened,
    this.externalId
  });

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'id': id,
      'first_time_opened': first_time_opened,
      'externalId' : externalId
    };
    return map;
  }

  static User fromMap(Map<String, dynamic> map) {
    return User(
        id: map['id'],
        first_time_opened: map['first_time_opened'],
        externalId: map['externalId']
    );
  }
}