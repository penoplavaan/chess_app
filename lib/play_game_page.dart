import 'dart:async';

import 'package:chess_app/screens/main_screen/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stateless_chessboard/flutter_stateless_chessboard.dart';
import 'package:sqflite/sqflite.dart';

import 'ContentFillers/statistics_content_filler.dart';
import 'Models/statistics.dart';
import 'Services/db.dart';
import 'utils.dart';

class PlayGamePage extends StatefulWidget {
  var moves;
  String orientColor = "white";
  int initialIndex;
  int depth;
  bool _isDemo;

  PlayGamePage(this.moves, this.orientColor,this.depth, this._isDemo,{Key? key, this.initialIndex = 0}) : super(key: key);

  @override
  _PlayGamePageState createState() => _PlayGamePageState(moves, orientColor,depth, _isDemo, initialIndex);
}

class _PlayGamePageState extends State<PlayGamePage> {
   var replayInitialIndex = 0;
   int errorIndex = 0;
   int depth;
   bool _isDemo;

  _PlayGamePageState(this.moves, this.orientColor, this.depth, this._isDemo, initialIndex) {
    moveIndex = initialIndex;
    replayInitialIndex = initialIndex;
    for (int i = 0; i < moves["movements"].split("|").length; i++) {
      fenArray.add(moves["movements"].split("|")[i]);
    }

    if (orientColor == "black") {
      moveIndex++;
      errorIndex++;
      fen = moves["movements"].split("|")[moveIndex];
      totalMovementsCount --;
    }

    //totalMovementsCount = moves["movements"].split("|").length - 2 - moveIndex;
    totalMovementsCount = depth*2 - moveIndex;
    fen = moves["movements"].split("|")[moveIndex];

    errorsList =  List.filled(totalMovementsCount, Icon(Icons.offline_pin, color: darkGreen,), growable: false);
    print(errorsList);
  }

  List errorsList = [];
  var moves;
  String orientColor = "white";
  List fenArray = [];

  String fen = "";
  int moveIndex = 0;
  int errorCount = 0;
  int totalMovementsCount = 0;

  Map<String, dynamic> orientation = {
    'white': BoardColor.WHITE,
    'black': BoardColor.BLACK
  };

  int errorsInRow = 0;

   bool _runTimer = false;

  final darkGreen = const Color.fromRGBO(116, 150, 90, 1);
  final lightGreen = const Color.fromRGBO(235, 239, 214, 1);
  final backColor = const Color.fromRGBO(32, 36, 33, 1);
  final gregColor = const Color.fromRGBO(237, 237, 237, 1);
  final changeColor = const Color.fromRGBO(226, 133, 24, 1);
  final deleteColor = const Color.fromRGBO(226, 24, 24, 1);

  int totalPlay = 0;
  int mistakes = 0;
  int wins = 0;
  int loses = 0;

  void initState(){
    showStatistics();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width;

    const addLeftPadding = 0;
    final topLettersPadding = width/8*7+width/64*5;
    final leftDigitsPadding = width - width/32;
    Color lightLetterColor = lightGreen;
    Color darkLetterColor = darkGreen;

    //top, left
    List whitePositionsArray = [
      //letters
      [topLettersPadding, width/8*0 + addLeftPadding],
      [topLettersPadding, width/8*1 + addLeftPadding],
      [topLettersPadding, width/8*2 + addLeftPadding],
      [topLettersPadding, width/8*3 + addLeftPadding],
      [topLettersPadding, width/8*4 + addLeftPadding],
      [topLettersPadding, width/8*5 + addLeftPadding],
      [topLettersPadding, width/8*6 + addLeftPadding],
      [topLettersPadding, width/8*7 + addLeftPadding],
      //digits
      [width/8*0, leftDigitsPadding],
      [width/8*1, leftDigitsPadding],
      [width/8*2, leftDigitsPadding],
      [width/8*3, leftDigitsPadding],
      [width/8*4, leftDigitsPadding],
      [width/8*5, leftDigitsPadding],
      [width/8*6, leftDigitsPadding],
      [width/8*7, leftDigitsPadding],
    ];

    //top, left
    List blackPositionsArray = [
      //letters
      [width/8*8-width/25, width/8*8-width/40],
      [width/8*8-width/25, width/8*7-width/40],
      [width/8*8-width/25, width/8*6-width/40],
      [width/8*8-width/25, width/8*5-width/40],
      [width/8*8-width/25, width/8*4-width/40],
      [width/8*8-width/25, width/8*3-width/40],
      [width/8*8-width/25, width/8*2-width/40],
      [width/8*8-width/25, width/8*1-width/40],
      //digits
      [width/8*8-width/25, 0.0],
      [width/8*7-width/25, 0.0],
      [width/8*6-width/25, 0.0],
      [width/8*5-width/25, 0.0],
      [width/8*4-width/25, 0.0],
      [width/8*3-width/25, 0.0],
      [width/8*2-width/25, 0.0],
      [width/8*1-width/25, 0.0],
    ];

    return WillPopScope(
      onWillPop: () async {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DismissibleApp()),
        );
        Future<bool> a = false as Future<bool>;
        return a;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(moves["name"].toString()),
              Text(moves["subName"].toString(), style: TextStyle(fontSize: 15),),
            ],
          ),
          backgroundColor: backColor,
          leading: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DismissibleApp()),
              );
            },
            child: Icon(
              Icons.arrow_back_outlined,
              color: Colors.white,
            ),
          ),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _isDemo? [
            SizedBox(width:30),
            FloatingActionButton(
              onPressed: () { makeAutomaticMove(direction: "backward"); },
              heroTag: 1,
              child: Icon(Icons.arrow_back_ios, color: darkGreen,),
              backgroundColor: lightGreen,
            ),
            FloatingActionButton(
              onPressed: () {
                setState(() {
                  _runTimer = !_runTimer;
                });
                Timer.periodic(Duration(milliseconds: 1200), (timer) {
                  if(_runTimer){
                    makeAutomaticMove();
                  }
                  else{
                    timer.cancel();
                  }
                });
                },
              child: Icon(_runTimer ? Icons.pause : Icons.play_arrow),
              heroTag: 2,
              backgroundColor: _runTimer ? changeColor : darkGreen,
            ),
            FloatingActionButton(
              onPressed: () { makeAutomaticMove(); },
              heroTag: 3,
              child: Icon(Icons.arrow_forward_ios, color: darkGreen),
              backgroundColor: lightGreen,
            ),
          ] : [],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: _isDemo? const EdgeInsets.only(top: 0, bottom: 12) : const EdgeInsets.only(top: 20, bottom: 12),
                child: _isDemo?  Container(): Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Осталось движений: ',
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      totalMovementsCount.toString(),
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )
                  ],
                ) ,
              ),
              Stack(
                children: [
                  Chessboard(
                  orientation: orientation[orientColor],
                  darkSquareColor: Color.fromRGBO(116, 150, 90, 1),
                  lightSquareColor: Color.fromRGBO(235, 239, 214, 1),
                  fen: fen,
                  size: size.width,
                  onMove: (move) {
                    print("moveIndex: " + moveIndex.toString());
                    print("errorIndex: $errorIndex");
                    print("totalMovementsCount: " + totalMovementsCount.toString());
                    final nextFen = makeMove(fen, {
                      'from': move.from,
                      'to': move.to,
                      'promotion': 'q',
                    });

                    if (nextFen != null) {
                      if (nextFen == fenArray[moveIndex + 1]) {
                        errorsInRow = 0;
                        moveIndex++;
                        errorIndex++;
                        totalMovementsCount--;
                        setState(() {
                          fen = nextFen;
                        });

                        Future.delayed(Duration(milliseconds: 300)).then((_) async {
                          final nextMove = fenArray[moveIndex + 1];
                          moveIndex++;
                          errorIndex++;
                          totalMovementsCount--;
                          print("after last dec: $totalMovementsCount");
                          if (nextMove != null && nextMove.length != 0 && totalMovementsCount!=-1) {
                            setState(() {
                              fen = nextMove;
                            });
                            if (nextMove.length != 0 && totalMovementsCount == 0) {

                              await _showEndDialog(errorCount, isDemo: _isDemo);
                              //ENDPOINT white
                            }
                          } else {

                            await _showEndDialog(errorCount, isDemo: _isDemo);
                            //ENDPOINT black
                          }
                        });
                      } else if (totalMovementsCount != 0) {
                        print("errorsInRow" + errorsInRow.toString());
                        setState(() {
                          errorsList[orientColor == "black" ?errorIndex-1:errorIndex]= Icon(Icons.error_outline, color: Colors.red,);
                          print(errorsList);
                          errorCount++;
                          if(errorsInRow == 2){
                            errorsInRow = 0;
                            makeAutomaticMove();
                          }
                          else {
                            errorsInRow++;
                          }
                          ScaffoldMessenger.of(context)
                            ..removeCurrentSnackBar()
                            ..showSnackBar(const SnackBar(
                            content: Text('Допущена ошибка'),
                            backgroundColor: Colors.red,
                          ));
                        });
                      }
                    } else {
                      errorsList[orientColor == "black" ?errorIndex-1:errorIndex] = Icon(Icons.error_outline, color: Colors.red,);
                      print("errorsInRow" + errorsInRow.toString());
                      setState(() {
                        errorCount++;
                        if(errorsInRow == 2){
                          errorsInRow = 0;
                          makeAutomaticMove();
                        }
                        else {
                          errorsInRow++;
                        }
                        ScaffoldMessenger.of(context)
                          ..removeCurrentSnackBar()
                          ..showSnackBar(const SnackBar(
                          content: Text(
                            'Такой ход невозможен...',
                            style: TextStyle(color: Colors.black),
                          ),
                          backgroundColor: Colors.yellow,
                        ));
                      });

                    }
                  },
                ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[0][0] : blackPositionsArray[0][0],
                        left: orientColor=="white" ? whitePositionsArray[0][1] : blackPositionsArray[0][1]
                    ),
                    child: Text("a", style: TextStyle(color: orientColor=="white" ? lightLetterColor : darkLetterColor),),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[1][0] : blackPositionsArray[1][0],
                        left: orientColor=="white" ? whitePositionsArray[1][1] : blackPositionsArray[1][1]
                    ),
                    child: Text("b", style: TextStyle(color: orientColor=="white" ? darkLetterColor : lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[2][0] : blackPositionsArray[2][0],
                        left: orientColor=="white" ? whitePositionsArray[2][1] : blackPositionsArray[2][1]
                    ),
                    child: Text("c", style: TextStyle(color: orientColor=="white" ? lightLetterColor : darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[3][0] : blackPositionsArray[3][0],
                        left: orientColor=="white" ? whitePositionsArray[3][1] : blackPositionsArray[3][1]
                    ),
                    child: Text("d", style: TextStyle(color:  orientColor=="white" ? darkLetterColor : lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[4][0] : blackPositionsArray[4][0],
                        left: orientColor=="white" ? whitePositionsArray[4][1] : blackPositionsArray[4][1]
                    ),
                    child: Text("e", style: TextStyle(color: orientColor=="white" ? lightLetterColor : darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[5][0] : blackPositionsArray[5][0],
                        left: orientColor=="white" ? whitePositionsArray[5][1] : blackPositionsArray[5][1]
                    ),
                    child: Text("f", style: TextStyle(color: orientColor=="white" ? darkLetterColor : lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[6][0] : blackPositionsArray[6][0],
                        left: orientColor=="white" ? whitePositionsArray[6][1] : blackPositionsArray[6][1]
                    ),
                    child: Text("g", style: TextStyle(color: orientColor=="white" ? lightLetterColor : darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[7][0] : blackPositionsArray[7][0],
                        left: orientColor=="white" ? whitePositionsArray[7][1] : blackPositionsArray[7][1]
                    ),
                    child: Text("h", style: TextStyle(color: orientColor=="white" ? darkLetterColor : lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[8][0] : blackPositionsArray[8][0],
                        left: orientColor=="white" ? whitePositionsArray[8][1] : blackPositionsArray[8][1]
                    ),
                    child: Text("8", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[9][0] : blackPositionsArray[9][0],
                        left: orientColor=="white" ? whitePositionsArray[9][1] : blackPositionsArray[9][1]
                    ),
                    child: Text("7", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[10][0] : blackPositionsArray[10][0],
                        left: orientColor=="white" ? whitePositionsArray[10][1] : blackPositionsArray[10][1]
                    ),
                    child: Text("6", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[11][0] : blackPositionsArray[11][0],
                        left: orientColor=="white" ? whitePositionsArray[11][1] : blackPositionsArray[11][1]
                    ),
                    child: Text("5", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[12][0] : blackPositionsArray[12][0],
                        left: orientColor=="white" ? whitePositionsArray[12][1] : blackPositionsArray[12][1]
                    ),
                    child: Text("4", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[13][0] : blackPositionsArray[13][0],
                        left: orientColor=="white" ? whitePositionsArray[13][1] : blackPositionsArray[13][1]
                    ),
                    child: Text("3", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[14][0] : blackPositionsArray[14][0],
                        left: orientColor=="white" ? whitePositionsArray[14][1] : blackPositionsArray[14][1]
                    ),
                    child: Text("2", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: orientColor=="white" ? whitePositionsArray[15][0] : blackPositionsArray[15][0],
                        left: orientColor=="white" ? whitePositionsArray[15][1] : blackPositionsArray[15][1]
                    ),
                    child: Text("1", style: TextStyle(color: darkLetterColor)),
                  ),
                ],
              ),
              _isDemo ?
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Text('Статистика', style: TextStyle(fontSize: 22),),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RichText(
                              text: TextSpan(
                                style: TextStyle(fontSize: 15, color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(text: 'Всего раз сыграно: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: '${totalPlay}'),
                                ],
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(fontSize: 15, color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(text: 'Всего ошибок: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: '${mistakes}'),
                                ],
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(fontSize: 15, color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(text: 'Процент побед: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: '${(wins/totalPlay*100).toStringAsFixed(2)}%'),
                                ],
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(fontSize: 15, color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(text: 'Побед: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: '${wins}'),
                                ],
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(fontSize: 15, color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(text: 'Поражений: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: '${loses}'),
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [

                          ],
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                        ],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                        ],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                        ],
                      )
                    ],
                  )
                ],
              ) :Container()
            ],
          ),
        ),
      ),
    );
  }

   Future updateStatistics(int openingId, int mistakes, bool winLose) async {
     Database db = await openDatabase(
         await getDatabasesPath() + StatisticsContentFiller.dbName);
     await db.execute(StatisticsContentFiller.onCreateString);
     DB dbInvariantOpeningManipulator = DB(
         db,
         StatisticsContentFiller.dbName,
         StatisticsContentFiller.tableName,
         StatisticsContentFiller.onCreateString);

     List statistics = (await dbInvariantOpeningManipulator.queryAll());
     bool currentOpeningIsFound = false;
     var currentOpening;
     int maxId = 0;
     if(statistics.isNotEmpty){
       statistics.forEach((el) {
         if(el['id'] > maxId) maxId = el['id'];
         if (el['openingId'] == openingId) {
           currentOpeningIsFound = true;
           currentOpening = el;
         }
       });
     }

     currentOpeningIsFound?
     dbInvariantOpeningManipulator.update(
         Statistics(
           id: currentOpening['id'],
           openingId: openingId,
           total_play: currentOpening['total_play']+1,
           mistakes: currentOpening['total_play']+mistakes,
           wins:  currentOpening['wins'] + (winLose?1:0),
           loses: currentOpening['loses']+ (winLose?0:1),
         )
     ) :
     dbInvariantOpeningManipulator.insert(
         Statistics(
           id: openingId,
           openingId: openingId,
           total_play: 1,
           mistakes: mistakes,
           wins:  winLose?1:0,
           loses: winLose?0:1,
         )
     );
   }

   Future showStatistics() async {
     Database db = await openDatabase(
         await getDatabasesPath() + StatisticsContentFiller.dbName);
     await db.execute(StatisticsContentFiller.onCreateString);
     DB dbInvariantOpeningManipulator = DB(
         db,
         StatisticsContentFiller.dbName,
         StatisticsContentFiller.tableName,
         StatisticsContentFiller.onCreateString);

     print(moves);
     var statistics = (await dbInvariantOpeningManipulator.query(moves["id"]))[0];
     print(statistics);
     totalPlay = statistics['total_play'];
     mistakes= statistics['mistakes'];
     wins = statistics['wins'];
     loses = statistics['loses'];
     setState((){});
     print('done ${totalPlay}');

   }


  Future<void> _showEndDialog(errors, {isDemo = false}) async {
    print(moves);
    if(!isDemo) await updateStatistics(moves["id"] ,errors, errors == 0);
    var dialog = AlertDialog(
      title: Text(errors == 0 ? 'Отличная работа!' : 'Тренируйтесь!'),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Text(errors == 0
                ? "Игра без ошибок"
                : 'Количество ошибок: ' + errors.toString()),
            Row(
              children: List.from(errorsList),
            ),
            SizedBox(
              width: 300,
              height: 300,
              child: errors == 0
                  ? Image.asset('assets/images/win1.png'):
              Image.asset('assets/images/lose1.png'),
            )
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Повторить',
            style: TextStyle(color: darkGreen),
          ),
          onPressed: () async {

            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PlayGamePage(moves, orientColor, depth, _isDemo,
                      initialIndex: replayInitialIndex)
              ),
            );
          },
        ),
        TextButton(
          child: const Text(
            'Домой',
          ),
          onPressed: () async {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DismissibleApp()),
            );
            //Navigator.of(context).pop();
          },
        ),
      ],
    );
    var demoDealog = AlertDialog(
      title: Text('Повторим?'),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            SizedBox(
              width: 300,
              height: 300,
              child: Image.asset('assets/images/win1.png')
            )
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Повторить',
            style: TextStyle(color: darkGreen),
          ),
          onPressed: () async {

            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PlayGamePage(moves, orientColor, depth, _isDemo,
                      initialIndex: replayInitialIndex)
              ),
            );
          },
        ),
        TextButton(
          child: const Text(
            'Домой',
          ),
          onPressed: () async {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DismissibleApp()),
            );
            //Navigator.of(context).pop();
          },
        ),
      ],
    );
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return isDemo?demoDealog:dialog;
      },
    );
  }

  Future<void> makeAutomaticMove({String direction = "forward"}) async {
    if(direction == "forward" && moveIndex + 1 < fenArray.length){
      Future.delayed(Duration(milliseconds: 300)).then((_) async {
        setState(() {
          fen = fenArray[moveIndex + 1];
        });

        moveIndex++;
        errorIndex++;
        totalMovementsCount--;
        Future.delayed(Duration(milliseconds: 300)).then((_) async {
          final nextMove = fenArray[moveIndex + 1];
          moveIndex++;
          errorIndex++;
          totalMovementsCount--;

          if (nextMove != null && nextMove.length != 0) {
            setState(() {
              fen = nextMove;
            });
            if (nextMove.length != 0 && totalMovementsCount == 0) {
              //ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Конец партии! (white)')));

              await _showEndDialog(errorCount, isDemo: _isDemo);
              //ENDPOINT white
            }
          } else {
            //ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Конец партии! (black)')));
            await _showEndDialog(errorCount, isDemo: _isDemo);
            //ENDPOINT black
          }
        });

      });
    }
    else if(direction == "backward" && moveIndex - 1 >= 0){
      Future.delayed(Duration(milliseconds: 300)).then((_) async {
        setState(() {
          fen = fenArray[moveIndex - 1];
        });

        moveIndex--;
        errorIndex--;
        totalMovementsCount++;
        Future.delayed(Duration(milliseconds: 300)).then((_) async {
          final nextMove = fenArray[moveIndex - 1];
          moveIndex--;
          errorIndex--;
          totalMovementsCount++;

          if (nextMove != null && nextMove.length != 0) {
            setState(() {
              fen = nextMove;
            });
            if (nextMove.length != 0 && totalMovementsCount == 0) {
              //ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Конец партии! (white)')));

              await _showEndDialog(errorCount, isDemo: _isDemo);
              //ENDPOINT white
            }
          } else {
            //ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Конец партии! (black)')));
            await _showEndDialog(errorCount, isDemo: _isDemo);
            //ENDPOINT black
          }
        });

      });
    }
  }
}
