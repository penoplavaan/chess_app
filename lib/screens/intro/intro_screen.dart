import 'package:chess_app/ContentFillers/user_content_filler.dart';
import 'package:chess_app/Models/user.dart';
import 'package:chess_app/Services/db.dart';
import 'package:chess_app/screens/main_screen/main_screen.dart';
import 'package:flutter/material.dart';
// import 'package:intro_slider/dot_animation_enum.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
// import 'package:intro_slider/slide_object.dart';
// import 'package:intro_slider/scrollbar_behavior_enum.dart';
import 'package:sqflite/sqflite.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;


class IntroScreen extends StatefulWidget {
  IntroScreen({Key? key}) : super(key: key);

  @override
  IntroScreenState createState() => new IntroScreenState();
}

// ------------------ Custom config ------------------
class IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = [];
  final darkGreen = const Color.fromRGBO(116, 150, 90, 1);
  final lightGreen = const Color.fromRGBO(235, 239, 214, 1);
  final backColor = const Color.fromRGBO(32, 36, 33, 1);
  final gregColor = const Color.fromRGBO(237, 237, 237, 1);
  final changeColor = const Color.fromRGBO(226, 133, 24, 1);
  final deleteColor = const Color.fromRGBO(226, 24, 24, 1);

  String login = '';
  String password = '';

  @override
  void initState() {
    super.initState();

    slides.add(
      Slide(
        title:
        "Добро пожаловать!",
        maxLineTitle: 2,
        styleTitle: const TextStyle(
          color: Colors.black,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'RobotoMono',
        ),
        description:
          "Нажмите на кнопку в правом нижнем углу, чтобы добавить новый дебют.",
        styleDescription: const TextStyle(
          color: Colors.black,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontFamily: 'Raleway',
        ),
        marginDescription:
        const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 70.0),
        centerWidget: const Image(image: AssetImage('assets/images/intro1.gif'),height: 400,),
        backgroundColor: Colors.white,
        directionColorBegin: Alignment.topLeft,
        directionColorEnd: Alignment.bottomRight,
        onCenterItemPress: () {},
      ),
    );
    slides.add(
      Slide(
        title:
        "Хотите добавить вариант?",
        maxLineTitle: 2,
        styleTitle: const TextStyle(
          color: Colors.black,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'RobotoMono',
        ),
        description:
        "Просто выделите его, и нажмите на левую нижнюю кнопку.",
        styleDescription: const TextStyle(
          color: Colors.black,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontFamily: 'Raleway',
        ),
        marginDescription:
        const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 70.0),
        centerWidget: const Image(image: AssetImage('assets/images/intro2.gif'),height: 400,),
        backgroundColor: Colors.white,
        directionColorBegin: Alignment.topLeft,
        directionColorEnd: Alignment.bottomRight,
        onCenterItemPress: () {},
      ),
    );
    slides.add(
      Slide(
        title:
        "Хотите удалить дебют?",
        maxLineTitle: 2,
        styleTitle: const TextStyle(
          color: Colors.black,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'RobotoMono',
        ),
        description:
        "Смахните его в списке влево!",
        styleDescription: const TextStyle(
          color: Colors.black,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontFamily: 'Raleway',
        ),
        marginDescription:
        const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 70.0),
        centerWidget: const Image(image: AssetImage('assets/images/intro3.gif'),height: 400,),
        backgroundColor: Colors.white,
        directionColorBegin: Alignment.topLeft,
        directionColorEnd: Alignment.bottomRight,
        onCenterItemPress: () {},
      ),
    );
    slides.add(
      Slide(
        title:
        "Хотите изменить дебют?",
        maxLineTitle: 2,
        styleTitle: const TextStyle(
          color: Colors.black,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'RobotoMono',
        ),
        description:
        "Смахните его в списке вправо!",
        styleDescription: const TextStyle(
          color: Colors.black,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontFamily: 'Raleway',
        ),
        marginDescription:
        const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 70.0),
        centerWidget: const Image(image: AssetImage('assets/images/intro4.gif'),height: 400,),
        backgroundColor: Colors.white,
        directionColorBegin: Alignment.topLeft,
        directionColorEnd: Alignment.bottomRight,
        onCenterItemPress: () {},
      ),
    );
    slides.add(
      Slide(
        title:
        "Вот и все!",
        maxLineTitle: 2,
        styleTitle: const TextStyle(
          color: Colors.black,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'RobotoMono',
        ),
        description:
        "Успехов!",
        styleDescription: const TextStyle(
          color: Colors.black,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontFamily: 'Raleway',
        ),
        marginDescription:
        const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 70.0),
        centerWidget: const Image(image: AssetImage('assets/images/win2.png'),height: 400,),
        backgroundColor: Colors.white,
        directionColorBegin: Alignment.topLeft,
        directionColorEnd: Alignment.bottomRight,
        onCenterItemPress: () {},
      ),
    );
    slides.add(
      Slide(
        title:
        "Войти в аккаунт",
        description:
        "Если у вас еще нет аккаунта,  он создастся автоматически",
        maxLineTitle: 2,
        styleTitle: const TextStyle(
          color: Colors.black,
          fontSize: 30.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'RobotoMono',
        ),
        styleDescription: const TextStyle(
          color: Colors.black,
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontFamily: 'Raleway',
        ),
        marginDescription: const EdgeInsets.only(
            left: 20.0, right: 20.0, top: 20.0, bottom: 70.0),
        centerWidget: Padding(
            padding: const EdgeInsets.all(10),
            child: ListView(
              scrollDirection: Axis.vertical,
                shrinkWrap: true,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    initialValue: login == "login"?"":login,
                    decoration: const InputDecoration(
                      helperText: 'Логин',
                    ),
                    onChanged: (value) {
                      login = value;
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextFormField(
                    initialValue: password == "password"?"":password,
                    decoration: const InputDecoration(
                      helperText: 'Пароль',
                    ),
                    onChanged: (value) {
                      password = value;
                    },
                  ),
                ),
                // Container(
                //     height: 50,
                //     padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                //     child: ElevatedButton(
                //       child: const Text('Войти'),
                //       onPressed: () {
                //         print(login);
                //         print(password);
                //       },
                //     )),
              ],
            )),
        backgroundColor: Colors.white,
        directionColorBegin: Alignment.topLeft,
        directionColorEnd: Alignment.bottomRight,
        onCenterItemPress: () {},
      ),
    );
  }

  Future<void> onDonePress() async {
    if(login != '' && password != ''){
      loginOrSignUp();
    }
    // await markAsOpened();
    Navigator.push(
       context,
      MaterialPageRoute(builder: (context) => DismissibleApp()),
     );
  }

  Future loginOrSignUp() async {
    Database db = await openDatabase(
        await getDatabasesPath() + UserContentFiller.dbName
    );
    await db.execute(UserContentFiller.onCreateString);
    DB dbUserManipulator = DB(
        db,
        UserContentFiller.dbName,
        UserContentFiller.tableName,
        UserContentFiller.onCreateString);

    var url = Uri.http('penoplavaan.space:3000', '/users');

    var response = await http.post(url, body: {
      'password': password,
      'login': login,
    });

    if (response.statusCode == 201) {
      var jsonResponse =
      convert.jsonDecode(response.body) as Map<String, dynamic>;
      User newUser = User(id: 1, first_time_opened: 0, externalId: jsonResponse['id']);
      dbUserManipulator.update(newUser);
    }
    else {
      print('Request failed with status: ${response.statusCode}  ${response.body}.');
    }
  }

  Future markAsOpened() async {
    Database db = await openDatabase(
        await getDatabasesPath() + UserContentFiller.dbName);
    await db.execute(UserContentFiller.onCreateString);
    DB dbUserManipulator = DB(
        db,
        UserContentFiller.dbName,
        UserContentFiller.tableName,
        UserContentFiller.onCreateString);

    User newUser = User(id: 1, first_time_opened: 0);
    dbUserManipulator.update(newUser);
  }

  void onNextPress() {
    print("onNextPress caught");
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      color: lightGreen,
      size: 35.0,
    );
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: lightGreen,
    );
  }

  Widget renderSkipBtn() {
    return Icon(
      Icons.skip_next,
      color: lightGreen,
    );
  }

  ButtonStyle myButtonStyle() {
    return ButtonStyle(
      shape: MaterialStateProperty.all<OutlinedBorder>(StadiumBorder()),
      backgroundColor: MaterialStateProperty.all<Color>(darkGreen),
      overlayColor: MaterialStateProperty.all<Color>(lightGreen),
    );
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      // List slides
      slides: slides,

      // Skip button
      renderSkipBtn: renderSkipBtn(),
      skipButtonStyle: myButtonStyle(),

      // Next button
      renderNextBtn: renderNextBtn(),
      onNextPress: onNextPress,
      nextButtonStyle: myButtonStyle(),

      // Done button
      renderDoneBtn: renderDoneBtn(),
      onDonePress: onDonePress,
      doneButtonStyle: myButtonStyle(),

      // Dot indicator
      colorDot: lightGreen,
      colorActiveDot: darkGreen,
      sizeDot: 13.0,

      // Show or hide status bar
      hideStatusBar: true,
      backgroundColorAllSlides: Colors.grey,

      // Scrollbar
      // verticalScrollbarBehavior: scrollbarBehavior.SHOW_ALWAYS,
    );
  }
}

// ------------------ Custom your own tabs ------------------
// class IntroScreenState extends State<IntroScreen> {
//   List<Slide> slides = [];
//
//   Function goToTab;
//
//   @override
//   void initState() {
//     super.initState();
//
//     slides.add(
//       new Slide(
//         title: "SCHOOL",
//         styleTitle: TextStyle(
//           color: Color(0xff3da4ab),
//           fontSize: 30.0,
//           fontWeight: FontWeight.bold,
//           fontFamily: 'RobotoMono',
//         ),
//         description:
//             "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",
//         styleDescription:
//             TextStyle(color: Color(0xfffe9c8f), fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
//         pathImage: "images/photo_school.png",
//       ),
//     );
//     slides.add(
//       new Slide(
//         title: "MUSEUM",
//         styleTitle:
//             TextStyle(color: Color(0xff3da4ab), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
//         description: "Ye indulgence unreserved connection alteration appearance",
//         styleDescription:
//             TextStyle(color: Color(0xfffe9c8f), fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
//         pathImage: "images/photo_museum.png",
//       ),
//     );
//     slides.add(
//       new Slide(
//         title: "COFFEE SHOP",
//         styleTitle:
//             TextStyle(color: Color(0xff3da4ab), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
//         description:
//             "Much evil soon high in hope do view. Out may few northward believing attempted. Yet timed being songs marry one defer men our. Although finished blessing do of",
//         styleDescription:
//             TextStyle(color: Color(0xfffe9c8f), fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
//         pathImage: "images/photo_coffee_shop.png",
//       ),
//     );
//   }
//
//   void onDonePress() {
//     // Back to the first tab
//     this.goToTab(0);
//   }
//
//   void onTabChangeCompleted(index) {
//     // Index of current tab is focused
//     print(index);
//   }
//
//   Widget renderNextBtn() {
//     return Icon(
//       Icons.navigate_next,
//       color: Color(0xffffcc5c),
//       size: 35.0,
//     );
//   }
//
//   Widget renderDoneBtn() {
//     return Icon(
//       Icons.done,
//       color: Color(0xffffcc5c),
//     );
//   }
//
//   Widget renderSkipBtn() {
//     return Icon(
//       Icons.skip_next,
//       color: Color(0xffffcc5c),
//     );
//   }
//
//   ButtonStyle myButtonStyle() {
//     return ButtonStyle(
//       shape: MaterialStateProperty.all<OutlinedBorder>(StadiumBorder()),
//       backgroundColor: MaterialStateProperty.all<Color>(Color(0x33ffcc5c)),
//       overlayColor: MaterialStateProperty.all<Color>(Color(0x33ffcc5c)),
//     );
//   }
//
//   List<Widget> renderListCustomTabs() {
//     List<Widget> tabs = [];
//     for (int i = 0; i < slides.length; i++) {
//       Slide currentSlide = slides[i];
//       tabs.add(Container(
//         width: double.infinity,
//         height: double.infinity,
//         child: Container(
//           margin: EdgeInsets.only(bottom: 60.0, top: 60.0),
//           child: ListView(
//             children: <Widget>[
//               GestureDetector(
//                   child: Image.asset(
//                 currentSlide.pathImage,
//                 width: 200.0,
//                 height: 200.0,
//                 fit: BoxFit.contain,
//               )),
//               Container(
//                 child: Text(
//                   currentSlide.title,
//                   style: currentSlide.styleTitle,
//                   textAlign: TextAlign.center,
//                 ),
//                 margin: EdgeInsets.only(top: 20.0),
//               ),
//               Container(
//                 child: Text(
//                   currentSlide.description,
//                   style: currentSlide.styleDescription,
//                   textAlign: TextAlign.center,
//                   maxLines: 5,
//                   overflow: TextOverflow.ellipsis,
//                 ),
//                 margin: EdgeInsets.only(top: 20.0),
//               ),
//             ],
//           ),
//         ),
//       ));
//     }
//     return tabs;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return new IntroSlider(
//       // Skip button
//       renderSkipBtn: this.renderSkipBtn(),
//       skipButtonStyle: myButtonStyle(),
//
//       // Next button
//       renderNextBtn: this.renderNextBtn(),
//       nextButtonStyle: myButtonStyle(),
//
//       // Done button
//       renderDoneBtn: this.renderDoneBtn(),
//       onDonePress: this.onDonePress,
//       doneButtonStyle: myButtonStyle(),
//
//       // Dot indicator
//       colorDot: Color(0xffffcc5c),
//       sizeDot: 13.0,
//       typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,
//
//       // Tabs
//       listCustomTabs: this.renderListCustomTabs(),
//       backgroundColorAllSlides: Colors.white,
//       refFuncGoToTab: (refFunc) {
//         this.goToTab = refFunc;
//       },
//
//       // Behavior
//       scrollPhysics: BouncingScrollPhysics(),
//
//       // Show or hide status bar
//       hideStatusBar: true,
//
//       // On tab change completed
//       onTabChangeCompleted: this.onTabChangeCompleted,
//     );
//   }
// }

// ------------------ Default config ------------------
// class IntroScreenState extends State<IntroScreen> {
//   List<Slide> slides = [];
//
//   @override
//   void initState() {
//     super.initState();
//
//     slides.add(
//       new Slide(
//         title: "ERASER",
//         description: "Allow miles wound place the leave had. To sitting subject no improve studied limited",
//         pathImage: "images/photo_eraser.png",
//         backgroundColor: Color(0xfff5a623),
//       ),
//     );
//     slides.add(
//       new Slide(
//         title: "PENCIL",
//         description: "Ye indulgence unreserved connection alteration appearance",
//         pathImage: "images/photo_pencil.png",
//         backgroundColor: Color(0xff203152),
//       ),
//     );
//     slides.add(
//       new Slide(
//         title: "RULER",
//         description:
//             "Much evil soon high in hope do view. Out may few northward believing attempted. Yet timed being songs marry one defer men our. Although finished blessing do of",
//         pathImage: "images/photo_ruler.png",
//         backgroundColor: Color(0xff9932CC),
//       ),
//     );
//   }
//
//   void onDonePress() {
//     // Do what you want
//     print("End of slides");
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return new IntroSlider(
//       slides: this.slides,
//       onDonePress: this.onDonePress,
//     );
//   }
// }