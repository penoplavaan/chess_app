import 'dart:convert';
import 'dart:ui';

import 'package:chess_app/ContentFillers/invariant_opening_content_fliier.dart';
import 'package:chess_app/ContentFillers/opening_variants_content_fliier.dart';
import 'package:chess_app/Helpers/chessboard_helper.dart' as cbh;
import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Services/db.dart';
import 'package:chess_app/screens/change_screen/change_screen.dart';
import 'package:chess_app/screens/intro/intro_screen.dart';
import 'package:chess_app/screens/new_opening/add_opening.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sortedmap/sortedmap.dart';
import 'package:sqflite/sqflite.dart';

import '../fake_home_page.dart';

class DismissibleApp extends StatefulWidget {
  @override
  DismissibleAppState createState() => new DismissibleAppState();
}

class DismissibleAppState extends State<DismissibleApp> {
  final darkGreen = const Color.fromRGBO(116, 150, 90, 1);
  final lightGreen = const Color.fromRGBO(235, 239, 214, 1);
  final backColor = const Color.fromRGBO(32, 36, 33, 1);
  final gregColor = const Color.fromRGBO(237, 237, 237, 1);
  final changeColor = const Color.fromRGBO(226, 133, 24, 1);
  final deleteColor = const Color.fromRGBO(226, 24, 24, 1);

  bool isMainBranchSelected = false;
  var widgetValues = [];
  List<Map<int, List<String>>> variants = [];

  var deleteBackground = Container(
    color: const Color.fromRGBO(226, 24, 24, 1),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: const [
        Padding(
          padding: EdgeInsets.only(right: 40.0),
          child: Icon(
            Icons.delete,
            color: Colors.black,
          ),
        )
      ],
    ),
  );
  var changeBackground = Container(
    color: const Color.fromRGBO(226, 133, 24, 1),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.only(left: 40.0),
          child: Icon(
            Icons.info_outline,
            color: Colors.black,
          ),
        )
      ],
    ),
  );

  DismissDirection direction = DismissDirection.startToEnd;

  @override
  void initState() {
    super.initState();
    isMainBranchSelected = true;
  }

  var allVariants = [];

  int _selectedIndex = 0;
  final List<String> _chosenSide = ['white', 'black'];

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Белые',
      style: optionStyle,
    ),
    Text(
      'Index 1: Черные',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: queryInvariantOpenings(_chosenSide[_selectedIndex]),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          var widgetToReturn;
          if (snapshot.hasData) {
            var moves = snapshot.data;
            widgetToReturn = WillPopScope(
              onWillPop: () async => false,
              child: Scaffold(
                appBar: AppBar(
                  title: const Text('Мои дебюты:'),
                  backgroundColor: backColor,
                  leading: FlatButton(
                    textColor: Colors.white,
                    onPressed: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => IntroScreen()),
                      // );
                    },
                    child: Icon(Icons.settings_system_daydream_rounded),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                  actions: <Widget>[
                    FlatButton(
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => IntroScreen()),
                        );
                      },
                      child: Text("Выйти"),
                      shape: CircleBorder(
                          side: BorderSide(color: Colors.transparent)),
                    ),
                  ],
                ),
                floatingActionButton: FloatingActionButton(
                  backgroundColor: backColor,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddOpeningPage()),
                    );
                  },
                  child: Text("+"),
                ),
                body: Padding(
                  padding: const EdgeInsets.all(0),
                  child: ListView.separated(
                      itemCount: moves.length,
                      separatorBuilder: (context, index) => const Divider(
                            color: Colors.white,
                            height: 0,
                          ),
                      itemBuilder: (context, index) {
                        return Dismissible(
                          key: ValueKey<int>(index),
                          background: changeBackground,
                          secondaryBackground: deleteBackground,
                          confirmDismiss: (DismissDirection direction) async {
                            print(direction);
                            if (direction == DismissDirection.startToEnd) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ChangeOpeningScreen(moves[index])),
                              );
                            } else {
                              await _showDeleteDialog(moves[index]["id"]);
                            }
                          },
                          onDismissed: (DismissDirection direction) async {
                            await deleteInvariantOpening(moves[index]["id"])
                                .then((value) => null);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DismissibleApp()),
                            );

                            /*
                            setState(() {
                              moves.removeAt(index);
                              print("DismissDirection: " + direction.toString());
                              //items.removeAt(index);
                            });*/
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 5, left: 3, right: 3),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(10),
                                color: index % 2 == 1 ? lightGreen : darkGreen,
                                //color: gregColor,
                              ),
                              child: Column(
                                children: [
                                  ListTile(
                                    isThreeLine: true,
                                    shape: const RoundedRectangleBorder(
                                      side: BorderSide(
                                        color: Colors.black87,
                                      ),
                                    ),
                                    title: Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10.0,
                                      ),
                                      child: SizedBox(
                                        child: Row(
                                          children: [
                                            Text(
                                              '${moves[index]["name"]}  ',
                                              style: TextStyle(
                                                  color: index % 2 == 0
                                                      ? lightGreen
                                                      : darkGreen,
                                                  fontSize: 24),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    subtitle: Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 0, horizontal: 0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "Вариант: " +
                                                moves[index]["subName"]
                                                    .toString(),
                                            style: TextStyle(
                                              color: index % 2 == 0
                                                  ? lightGreen
                                                  : darkGreen,
                                              fontSize: 16,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            "Ходов: " +
                                                ((moves[index]["movements"]
                                                                .split("|")
                                                                .length -
                                                            2) /
                                                        2)
                                                    .round()
                                                    .toString(),
                                            style: TextStyle(
                                                color: index % 2 == 0
                                                    ? lightGreen
                                                    : darkGreen,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "Движений: " +
                                                (moves[index]["movements"]
                                                            .split("|")
                                                            .length -
                                                        2)
                                                    .toString(),
                                            style: TextStyle(
                                                color: index % 2 == 0
                                                    ? lightGreen
                                                    : darkGreen,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          SizedBox(
                                            width: 360,
                                            height: 24,
                                            child: Text(
                                              moves[index]["movementsTextual"]
                                                  .toString(),
                                              style: TextStyle(
                                                fontSize: 18,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    trailing: Text(
                                      moves[index]["attachedVariants"]
                                                  .toString()
                                                  .length ==
                                              0
                                          ? "Вариантов: 0"
                                          : "Вариантов: " +
                                              moves[index]["attachedVariants"]
                                                  .toString()
                                                  .split("|")
                                                  .length
                                                  .toString(),
                                      style: TextStyle(
                                          color: index % 2 == 0
                                              ? lightGreen
                                              : darkGreen,
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    onTap: () {
                                      int clickIndex = index;

                                      print("clickIndex: " +
                                          clickIndex.toString());
                                      List<String> attachedVariantsIDs =
                                          moves[index]["attachedVariants"]
                                              .toString()
                                              .split("|");
                                      print(moves[index]);
                                      Map<int, List<Map<String, String>>> data =
                                          loadAllIndexes(
                                              attachedVariantsIDs,
                                              allVariants,
                                              moves[index]['name']);
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return BackdropFilter(
                                              filter: ImageFilter.blur(
                                                  sigmaX: 10, sigmaY: 10),
                                              child: AlertDialog(
                                                content: Stack(
                                                  clipBehavior: Clip.none,
                                                  children: <Widget>[
                                                    Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  bottom: 20.0),
                                                          child: Row(
                                                            children: [
                                                              Text(
                                                                data.length == 0
                                                                    ? 'Вариантов нет'
                                                                    : 'Выберите ветку или вариант',
                                                                style: const TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    decoration:
                                                                        TextDecoration
                                                                            .underline),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        /*
                                                        Container(
                                                          decoration: BoxDecoration(
                                                            color: lightGreen,
                                                            border: Border.all(
                                                                color:
                                                                    Colors.black),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(10),
                                                          ),
                                                          child: ListTile(
                                                              onTap: () {
                                                                //goToFakeHomePage(context, moves[clickIndex]);
                                                                Navigator.push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                        builder: (context) =>
                                                                            FakeHomePage(
                                                                                moves[clickIndex])));
                                                              },
                                                              shape: RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10.0)),
                                                              minVerticalPadding: 0,
                                                              title: const Text(
                                                                'Основная ветка',
                                                                style: TextStyle(
                                                                    fontSize: 20,
                                                                    color: Colors
                                                                        .black),
                                                              ),
                                                              subtitle: Text(
                                                                moves[index][
                                                                    "movementsTextual"],
                                                                style: TextStyle(
                                                                    color:
                                                                        backColor),
                                                              ),
                                                              trailing: Icon(Icons.play_arrow, size: 40.0, color: darkGreen,)
                                                          ),
                                                        ),
                                                        */
                                                        SizedBox(
                                                          height:
                                                              data.length == 0
                                                                  ? 0
                                                                  : 200,
                                                          width: 300,
                                                          child:
                                                              ListView.builder(
                                                                  //itemCount: data.length,
                                                                  itemCount: data
                                                                      .length,
                                                                  itemBuilder:
                                                                      (context,
                                                                          index) {
                                                                    Column
                                                                        widgetToReturn;

                                                                    String? key = data
                                                                        .keys
                                                                        .elementAt(
                                                                            index)
                                                                        .toString();
                                                                    var items = data
                                                                        .values
                                                                        .elementAt(
                                                                            index);

                                                                    List<DropdownMenuItem<String>>
                                                                        dropdownMenuItems =
                                                                        [];

                                                                    for (var item
                                                                        in items) {
                                                                      //print('cbh.ChessboardHelper.importanceBackColors[item["importance"]] ${cbh.ChessboardHelper.importanceBackColors[int.parse(item["importance"].toString()) ]}');
                                                                      dropdownMenuItems.add(DropdownMenuItem(
                                                                          value: item.values.join("%"),
                                                                          child: Text(
                                                                            item["movementsTextual"].toString().length > 30
                                                                                ? item["movementsTextual"].toString().substring(0, 30) + "...."
                                                                                : item["movementsTextual"].toString(),
                                                                            style:
                                                                                TextStyle(color: cbh.ChessboardHelper.importanceBackColors[int.parse(item["importance"].toString())]),
                                                                          )));
                                                                    }

                                                                    widgetValues.add(
                                                                        dropdownMenuItems[0]
                                                                            .value);
                                                                    widgetValues[
                                                                            index] =
                                                                        dropdownMenuItems[0]
                                                                            .value;
                                                                    widgetToReturn =
                                                                        Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Padding(
                                                                          padding:
                                                                              const EdgeInsets.only(bottom: 8.0),
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Row(
                                                                                children: [
                                                                                  Text(
                                                                                    "Ход " + key.toString(),
                                                                                    style: TextStyle(fontSize: 20),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                                                                SizedBox(
                                                                                  width: 260,
                                                                                  height: 50,
                                                                                  child: DropdownButtonFormField<String>(
                                                                                    dropdownColor: gregColor,
                                                                                    value: widgetValues[index],
                                                                                    icon: const Icon(Icons.keyboard_arrow_down),
                                                                                    items: dropdownMenuItems,
                                                                                    onChanged: (String? newValue) {
                                                                                      setState(() {
                                                                                        print("newVal:" + newValue!);
                                                                                        widgetValues[index] = newValue;
                                                                                        isMainBranchSelected = !isMainBranchSelected;

                                                                                        Map<String, dynamic> object = {
                                                                                          'name': newValue.split('%')[0],
                                                                                          'movements': newValue.split('%')[1],
                                                                                          'movementsTextual': newValue.split('%')[2],
                                                                                        };

                                                                                        var blackOption = object['movementsTextual']!.split("...").length == 2;

                                                                                        print("___OBJECT___, clickIndex: " + clickIndex.toString());
                                                                                        object["id"] = clickIndex.toString();
                                                                                        object.addAll({
                                                                                          "id": clickIndex
                                                                                        });

                                                                                        print(object);

                                                                                        Navigator.push(
                                                                                            context,
                                                                                            MaterialPageRoute(
                                                                                                builder: (context) => FakeHomePage(
                                                                                                      object,
                                                                                                      blackOption: blackOption,
                                                                                                    )));
                                                                                      });
                                                                                    },
                                                                                  ),
                                                                                ),
                                                                              ])
                                                                            ],
                                                                          ),
                                                                        )
                                                                      ],
                                                                    );
                                                                    return widgetToReturn;
                                                                  }),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(0.0),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              ElevatedButton(
                                                                child: Icon(
                                                                    Icons
                                                                        .close),
                                                                style:
                                                                    ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStateProperty.all<
                                                                              Color>(
                                                                          deleteColor),
                                                                ),
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                              ),
                                                              Container(
                                                                width: 140,
                                                                height: 45,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color:
                                                                      lightGreen,
                                                                  border: Border.all(
                                                                      color: Colors
                                                                          .black),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10),
                                                                ),
                                                                child: ListTile(
                                                                    onTap: () {
                                                                      //goToFakeHomePage(context, moves[clickIndex]);
                                                                      Navigator.push(
                                                                          context,
                                                                          MaterialPageRoute(
                                                                              builder: (context) => FakeHomePage(moves[clickIndex])));
                                                                    },
                                                                    title: Text(
                                                                      data.length ==
                                                                              0
                                                                          ? 'Вперед'
                                                                          : 'Основная',
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              14,
                                                                          color:
                                                                              Colors.black),
                                                                    ),
                                                                    trailing:
                                                                        Icon(
                                                                      Icons
                                                                          .play_arrow,
                                                                      size:
                                                                          20.0,
                                                                      color:
                                                                          darkGreen,
                                                                    ),
                                                                    dense:
                                                                        true),
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          });
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
                bottomNavigationBar: BottomNavigationBar(
                  items: const <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: Icon(Icons.circle_outlined),
                      label: 'Белые',
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.circle_rounded),
                      label: 'Черные',
                    ),
                  ],
                  currentIndex: _selectedIndex,
                  selectedItemColor: darkGreen,
                  onTap: _onItemTapped,
                ),
              ),
            );
          } else {
            widgetToReturn = Scaffold(
              appBar: AppBar(
                title: const Text('Мои дебюты:'),
                backgroundColor: backColor,
              ),
              floatingActionButton: FloatingActionButton(
                backgroundColor: backColor,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AddOpeningPage()),
                    //builder: (context) => HomePage('RNBKQBNR/PPPPPPPP/8/8/8/8/pppppppp/rnbkqbnr b KQkq - 0 1')),
                  );
                },
                child: Text("+"),
              ),
            );
          }
          ;

          return widgetToReturn;
        });
  }

  Future queryInvariantOpenings(String chosenSide) async {
    Database db = await openDatabase(
        await getDatabasesPath() + InvariantOpeningContentFiller.dbName);
    await db.execute(InvariantOpeningContentFiller.onCreateString);
    await db.execute(VariantOfOpeningContentFiller.onCreateString);

    DB dbInvariantOpeningManipulator = DB(
        db,
        InvariantOpeningContentFiller.dbName,
        InvariantOpeningContentFiller.tableName,
        InvariantOpeningContentFiller.onCreateString);

    DB dbVariantsManipulator = DB(
        db,
        VariantOfOpeningContentFiller.dbName,
        VariantOfOpeningContentFiller.tableName,
        VariantOfOpeningContentFiller.onCreateString);

    allVariants = await dbVariantsManipulator.queryAll();

    try {
      var response =
          await dbInvariantOpeningManipulator.queryAllBySide(chosenSide);

      var localOpenings =
          await dbInvariantOpeningManipulator.queryAllBySideOffline(chosenSide);

      if (response.statusCode == 200) {
        var cloudOpenings = json.decode(utf8.decode(response.bodyBytes));

        syncInvariantOpeningsIntoLocalStorage(
            localOpenings, cloudOpenings, dbInvariantOpeningManipulator);
        return cloudOpenings;
      } else {
        return await dbInvariantOpeningManipulator
            .queryAllBySideOffline(chosenSide);
      }
    } catch (e) {
      return await dbInvariantOpeningManipulator
          .queryAllBySideOffline(chosenSide);
    }
  }

  Future syncInvariantOpeningsIntoLocalStorage(List<dynamic> localOpenings,
      List<dynamic> cloudOpenings, DB dbInvariantOpeningManipulator) async {
    print(localOpenings.isEmpty);
    print(localOpenings.length);

    if (localOpenings.isEmpty) {
      cloudOpenings.forEach((element) {
        dbInvariantOpeningManipulator.insert(InvariantOpening(
          id: 1,
          name: element['name'],
          subName: element['subName'],
          movements: element['movements'],
          movementsTextual: element['movementsTextual'],
          attachedVariants: element['attachedVariants'],
          side: element['side'],
        ));
      });
    }
  }

  Future deleteInvariantOpening(index) async {
    try {
      var url = Uri.http('penoplavaan.space:3000', '/openings/$index');

      await http.delete(url);
    } catch (e) {
      Database db = await openDatabase(
          await getDatabasesPath() + InvariantOpeningContentFiller.dbName);
      await db.execute(InvariantOpeningContentFiller.onCreateString);
      DB dbInvariantOpeningManipulator = DB(
          db,
          InvariantOpeningContentFiller.dbName,
          InvariantOpeningContentFiller.tableName,
          InvariantOpeningContentFiller.onCreateString);

      await dbInvariantOpeningManipulator.delete(index);
    }
  }

  Future<void> _showDeleteDialog(id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Удалить запись?'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Внимание!'),
                Text('Эту операцию неальзя отменить!'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Отмена'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text(
                'Удалить',
                style: TextStyle(color: Colors.red),
              ),
              //moves[index]["id"]
              onPressed: () async {
                await deleteInvariantOpening(id).then((value) => null);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DismissibleApp()),
                );
                //Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showDeleteOrChangeDialog(moves, id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text(''),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        child: Stack(
                          children: <Widget>[
                            Positioned.fill(
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(4),
                                  //color: gregColor,
                                ),
                              ),
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                padding: const EdgeInsets.all(16.0),
                                primary: Colors.white,
                                textStyle: const TextStyle(fontSize: 16),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ChangeOpeningScreen(moves)),
                                );
                              },
                              child: const Text(
                                'Изменить',
                                style: TextStyle(color: Colors.amber),
                              ),
                            ),
                          ],
                        ),
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        child: Stack(
                          children: <Widget>[
                            Positioned.fill(
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(4),
                                  //color: gregColor,
                                ),
                              ),
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                padding: const EdgeInsets.all(16.0),
                                primary: Colors.white,
                                textStyle: const TextStyle(fontSize: 16),
                              ),
                              onPressed: () async {
                                await _showDeleteDialog(id);
                              },
                              child: const Text(
                                'Удалить',
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Отмена'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Map<int, List<Map<String, String>>> loadAllIndexes(
      attachedVariantsIDs, allVariants, name) {
    Map<int, List<Map<String, String>>> queriedVariants = {};
    //print(attachedVariantsIDs.length);
    if (attachedVariantsIDs[0] != "") {
      attachedVariantsIDs.forEach((id) {
        allVariants.forEach((variant) {
          if (variant["id"] == int.parse(id)) {
            if (queriedVariants[variant["attachToIndex"]] == null) {
              queriedVariants[variant["attachToIndex"]] = [];
            }
            queriedVariants[variant["attachToIndex"]]!.add({
              "name": name.toString(),
              "movements": variant["movements"],
              "movementsTextual": variant["movementsTextual"],
              "importance": variant["importance"].toString(),
            });
          }
        });
      });
    }
    var orderedQueriedVariants = SortedMap(Ordering.byKey());
    orderedQueriedVariants.addAll(queriedVariants);
    queriedVariants = orderedQueriedVariants.cast();

    return queriedVariants;
  }
}
