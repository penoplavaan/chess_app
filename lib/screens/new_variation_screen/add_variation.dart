import 'package:chess_app/ContentFillers/invariant_opening_content_fliier.dart';
import 'package:chess_app/ContentFillers/opening_variants_content_fliier.dart';
import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Models/variant_of_opening.dart';
import 'package:chess_app/Services/db.dart';
import 'package:chess_app/screens/main_screen/main_screen.dart';
import 'package:chess_app/screens/new_opening/add_opening.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stateless_chessboard/flutter_stateless_chessboard.dart';
import 'package:sqflite/sqflite.dart';
import 'package:chess_app/Helpers/chessboard_helper.dart' as cbh;
import '../../ContentFillers/variant_commentary_content_fliier.dart';
import '../../Models/variant_commentary.dart';
import '../../utils.dart';

class AddVariationPage extends StatefulWidget {
  int indexToAttach = 1;
  String fen = "";
  int moveIndex = 0;
  BoardColor orientation;
  AddVariationPage(this.indexToAttach ,this.fen, this.moveIndex, this.orientation);
  @override
  _AddVariationPageState createState() => _AddVariationPageState(indexToAttach, fen, moveIndex, orientation);

}

class _AddVariationPageState extends State<AddVariationPage> {
  _AddVariationPageState(this.indexToAttach, fen, this.moveIndex, this.orientation){
    this._fen = fen;
    this.steps = fen+"|";

    print("indexToAttach : " + indexToAttach.toString());

    emptyFieldsCount = getCurrEmptyFieldsCount(_fen);
  }
  int indexToAttach = 1;
  BoardColor orientation;
  String _fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
  final darkGreen = const Color.fromRGBO(116, 150, 90, 1);
  final lightGreen = const Color.fromRGBO(235, 239, 214, 1);
  final backColor = const Color.fromRGBO(32, 36, 33, 1);
  final gregColor = const Color.fromRGBO(237, 237, 237, 1);
  final changeColor = const Color.fromRGBO(226, 133, 24, 1);
  final deleteColor = const Color.fromRGBO(226, 24, 24, 1);
  String steps = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1|";
  String figures = "rnbqkpRNBQKP";

  String newOpeningName = "";
  String stepsTextual = "";
  List<Container> movesTextChildren = [];

  final List<List<Color>> _colors = cbh.ChessboardHelper.highliteMoveColors;

  final List<List<Border>> _borders =  cbh.ChessboardHelper.highliteMoveBorders;

  final List<List<Color>> _textColors = cbh.ChessboardHelper.highliteMoveTextColors;

  final ScrollController _scrollController = ScrollController();

  int emptyFieldsCount = 32;
  int moveIndex = 0;

  int undoIndex = 0;
  List<bool> isSelected = [true, false,false];
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width;

    const addLeftPadding = 3;
    const addTopPadding = 3;
    final topLettersPadding = width/8*7+width/64*5;
    final leftDigitsPadding = width - width/32;
    Color lightLetterColor = lightGreen;
    Color darkLetterColor = darkGreen;

    return Scaffold(
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 0,right: 40),
            child: ToggleButtons(
                children: [
                  Text("1"),
                  Text("2"),
                  Text("3"),
                ],
              selectedColor: getSelectedTextColor(isSelected),
              fillColor: getSelectedFillColor(isSelected),
              onPressed: (int index) {

                  for (int buttonIndex = 0; buttonIndex < isSelected.length; buttonIndex++) {
                    if (buttonIndex == index) {
                      isSelected[buttonIndex] = true;
                    } else {
                      isSelected[buttonIndex] = false;
                    }
                  }
                  setState(() {});
              },
              isSelected: isSelected,
            ),
          ),
          OutlinedButton(
              style: OutlinedButton.styleFrom(
                shape: CircleBorder(),
                padding: EdgeInsets.all(20),
                backgroundColor: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  List<String> movesList = stepsTextual.split(" ");
                  List<String> lastMoveList =
                  movesList[stepsTextual.split(" ").length - 1]
                      .split(" ");

                  if (lastMoveList.length == 3) {
                    lastMoveList.removeLast();
                    movesList[stepsTextual.split(" ").length - 1] =
                        lastMoveList.join(" ");
                  } else {
                    movesList.removeLast();
                    lastMoveList = [];
                  }

                  stepsTextual = movesList.join(" ");
                  movesTextChildren = updateMovesText(stepsTextual);

                  moveIndex--;
                  undoIndex --;

                  var ts = steps.split("|");
                  ts.removeLast();
                  ts.removeLast();
                  steps = ts.join("|") + "|";
                  print("indexToAttach" + indexToAttach.toString());
                  _fen = steps.split("|")[undoIndex];

                  emptyFieldsCount = getCurrEmptyFieldsCount(_fen);

                });
              },
              child: Icon(Icons.undo, color: Colors.black,)
          ),
        ],
      ),
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              height: 70,
              width: 200,
            ),
            ElevatedButton(
              onPressed: () async {

                var variants = await (addNewVariant(indexToAttach, steps, stepsTextual));

                int newVariantId = variants.toList().last["id"];
                Navigator.pop(context, newVariantId);
              },
              child: Icon(Icons.save),
              style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all(Colors.transparent)),
            )
          ],
        ),
        backgroundColor: backColor,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 0.0),
              child: Stack(
                children: [
                  Chessboard(
                  darkSquareColor: Color.fromRGBO(116, 150, 90, 1),
                  lightSquareColor: Color.fromRGBO(235, 239, 214, 1),
                  fen: _fen,
                  orientation: orientation,
                  size: size.width,
                  onMove: (move) {
                    _scrollController.animateTo(
                        _scrollController.position.maxScrollExtent+100,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.ease);
                    String moveFrom = move.from;
                    String moveTo = move.to;
                    String prevFen = _fen;
                    final nextFen = makeMove(_fen, {
                      'from': move.from,
                      'to': move.to,
                      'promotion': 'q',
                    });

                    if (nextFen != null) {
                      moveIndex++;
                      undoIndex++;
                      steps += nextFen + "|";
                      setState(() {
                        _fen = nextFen;
                        String currFen = _fen;
                        String figFromColor = prevFen.split(" ")[1];

                        int currEmptyFieldsCount =
                        getCurrEmptyFieldsCount(currFen);

                        String moveTextual =
                        currEmptyFieldsCount == emptyFieldsCount
                            ? getMovedFigure(currFen.split(" ")[0], moveTo) +
                            moveTo
                            : validateMovedFigure(currFen, moveFrom, moveTo) +
                            "×" +
                            moveTo;
                        movesTextChildren =
                            updateMovesText(this.stepsTextual);


                        emptyFieldsCount = currEmptyFieldsCount;
                        if (moveFrom == 'e1' &&
                            moveTo == 'c1' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♔') {
                          stepsTextual +=
                              " " + ((moveIndex - moveIndex % 2) / 2 + 1)
                                  .round().toString() + ". " + "0-0-0";

                        } else if (moveFrom == 'e1' &&
                            moveTo == 'g1' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♔') {
                          stepsTextual +=
                              " " + ((moveIndex - moveIndex % 2) / 2 + 1)
                                  .round().toString() + ". " + "0-0";

                        } else if (moveFrom == 'e8' &&
                            moveTo == 'c8' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♚') {
                          stepsTextual += " 0-0-0";

                        } else if (moveFrom == 'e8' &&
                            moveTo == 'g8' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♚') {
                          stepsTextual += " 0-0";

                        }

                        else {
                          if (figFromColor == "w") {
                            stepsTextual +=
                                " " + ((moveIndex - moveIndex % 2) / 2 + 1)
                                    .round().toString() + ". " + moveTextual;
                          print("white step");
                          } else {
                            if (stepsTextual.split(" ").length==1){
                              stepsTextual = " " + (((moveIndex - moveIndex % 2) / 2 + 1)
                                  .round()-1).toString()+ ". ... " + moveTextual;
                            }
                            else{
                              stepsTextual += " " + moveTextual;
                            }


                            print("black step " + stepsTextual.split(" ").toString());
                          }
                        }
                        movesTextChildren =
                            updateMovesText(this.stepsTextual);
                      });
                    }
                  },
                ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*0 + addLeftPadding
                    ),
                    child: Text("a", style: TextStyle(color: lightLetterColor),),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*1+ addLeftPadding
                    ),
                    child: Text("b", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*2+ addLeftPadding
                    ),
                    child: Text("c", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*3+ addLeftPadding
                    ),
                    child: Text("d", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*4+ addLeftPadding
                    ),
                    child: Text("e", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*5+ addLeftPadding
                    ),
                    child: Text("f", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*6+ addLeftPadding
                    ),
                    child: Text("g", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: topLettersPadding,
                        left: width/8*7+ addLeftPadding
                    ),
                    child: Text("h", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*0,
                        left: leftDigitsPadding
                    ),
                    child: Text("8", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*1,
                        left: leftDigitsPadding
                    ),
                    child: Text("7", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*2,
                        left: leftDigitsPadding
                    ),
                    child: Text("6", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*3,
                        left: leftDigitsPadding
                    ),
                    child: Text("5", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*4,
                        left: leftDigitsPadding
                    ),
                    child: Text("4", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*5,
                        left: leftDigitsPadding
                    ),
                    child: Text("3", style: TextStyle(color: darkLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*6,
                        left: leftDigitsPadding
                    ),
                    child: Text("2", style: TextStyle(color: lightLetterColor)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: width/8*7,
                        left: leftDigitsPadding
                    ),
                    child: Text("1", style: TextStyle(color: darkLetterColor)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromRGBO(235, 239, 214, 1),
                ),
                width: 450,
                child: SafeArea(
                  child: SingleChildScrollView(
                    controller: _scrollController,
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 5, left: 12, right: 12, bottom: 5
                            ),
                            child: Container(
                              color: Color.fromRGBO(235, 239, 214, 1),
                              //child: Text(stepsTextual,style: TextStyle(fontSize: 22))
                              child: Container(
                                //child: Text(stepsTextual,style: TextStyle(fontSize: 22))
                                  child: Row(
                                    children: List<Container>.from(movesTextChildren),
                                  )),),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }



  Future queryInvariantOpenings() async {
    Database db = await openDatabase(
        await getDatabasesPath() + InvariantOpeningContentFiller.dbName);
    await db.execute(InvariantOpeningContentFiller.onCreateString);
    DB dbInvariantOpeningManipulator = DB(
        db,
        InvariantOpeningContentFiller.dbName,
        InvariantOpeningContentFiller.tableName,
        InvariantOpeningContentFiller.onCreateString);

    return dbInvariantOpeningManipulator.queryAll();
  }

  Future addNewVariant(name, steps, movementsTextual) async {
    Database db = await openDatabase(
        await getDatabasesPath() + VariantOfOpeningContentFiller.dbName);
    await db.execute(VariantOfOpeningContentFiller.onCreateString);
    DB dbVariantOfOpeningManipulator = DB(
        db,
        VariantOfOpeningContentFiller.dbName,
        VariantOfOpeningContentFiller.tableName,
        VariantOfOpeningContentFiller.onCreateString);

    dbVariantOfOpeningManipulator.insert(OpeningVariant(
        id: 1,
        attachToIndex: indexToAttach,
        movements: steps,
        movementsTextual: movementsTextual,
        importance: isSelected.indexOf(true)
    )
    );

    return await dbVariantOfOpeningManipulator.queryAll();
  }



  String getMovedFigure(String currFen, String moveTo) {
    List<String> digits = ['1', '2', '3', '4', '5', '6', '7', '8'];
    Map<String, int> mapToGetIndex = {
      "a": 0,
      "b": 1,
      "c": 2,
      "d": 3,
      "e": 4,
      "f": 5,
      "g": 6,
      "h": 7
    };

    Map<String, dynamic> mapNameSymbol = {
      "K": "♔",
      "Q": "♕",
      "R": "♖",
      "B": "♗",
      "N": "♘",
      "k": "♚",
      "q": "♛",
      "r": "♜",
      "b": "♝",
      "n": "♞",
    };

    List moveToList = [
      8 - int.parse(moveTo.split('')[1]),
      mapToGetIndex[moveTo.split('')[0]]
    ];
    var fieldList = currFen.split('/').map((e) {
      String ourString = e;
      String newString = "";

      for (int i = 0; i < e.length; i++) {
        if (digits.contains(e[i])) {
          newString += '1' * int.parse(ourString[i]);
        } else {
          newString += ourString[i];
        }
      }
      return newString.split('');
    }).toList();

    String valueToReturn = fieldList[moveToList[0]][moveToList[1]];
    return valueToReturn.toLowerCase() == 'p'
        ? ""
        : mapNameSymbol[valueToReturn];
  }

  int getCurrEmptyFieldsCount(String fen) {
    int currEmptyFieldsCount = 0;

    List fenElements = fen.split(" ")[0].split("/").join("").split("");
    fenElements.forEach((element) {
      if (isNumeric(element)) {
        currEmptyFieldsCount += int.parse(element);
      }
    });

    print("currEmptyFieldsCount" + currEmptyFieldsCount.toString());
    print("emptyFieldsCount"+emptyFieldsCount.toString());
    return currEmptyFieldsCount;

  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }



  String validateMovedFigure(currFen, moveFrom, moveTo) {
    return getMovedFigure(currFen.split(" ")[0], moveTo) == ""
        ? moveFrom.split("")[0]
        : getMovedFigure(currFen.split(" ")[0], moveTo);
  }


  List<Container> updateMovesText(moves) {

    List<Container> movesTextChildren = [];
    for (var i = 1; i < moves.split(" ").length; i++) {
      var moveParts = moves.split(" ")[i].split(" ");
      movesTextChildren.add(
          Container(
            child: Text("  " + moveParts[0].toString() + " ",
                style: const TextStyle(
                  color: Colors.grey,
                  fontSize: 20,
                )
            ),
          )
      );
      //
      movesTextChildren.add(
          Container(
            decoration: BoxDecoration(
              border: _borders[int.parse(moveParts[0].split("")[0]) - 1][0],
              borderRadius: BorderRadius.circular(10),
              color: _colors[int.parse(moveParts[0].split("")[0]) - 1][0],
            ),

            child: InkWell(
                onTap: () {
                  print("__MOVE PARTS___");
                  print(moveParts);
                  int index = int.parse(moveParts[0].split("")[0]);
                  print("___INDEX___ "+index.toString());
                  print("___STEPS___");
                  print(steps);
                  print("___MOVE TO INDEX___"+(index*2-1-indexToAttach).toString());
                  print("___indexToAttach___"+indexToAttach.toString());
                  String currFen = steps.split("|")[index*2-1-indexToAttach];

                  setState(() {
                    _fen = currFen;

                    _colors.forEach((row){
                      row[0] = Colors.transparent;
                      row[1] = Colors.transparent;
                    });

                    _borders.forEach((row){
                      row[0] = Border.all(color: Colors.transparent);
                      row[1] = Border.all(color: Colors.transparent);
                    });

                    _textColors.forEach((row){
                      row[0] = Colors.black;
                      row[1] = Colors.black;
                    });

                    _borders[int.parse(moveParts[0].split('')[0])-1][0] =  Border.all(color: Colors.black);
                    _colors[int.parse(moveParts[0].split('')[0])-1][0] =  darkGreen;
                    _textColors[int.parse(moveParts[0].split('')[0])-1][0] =  Colors.white;
                  });
                  this.movesTextChildren = updateMovesText(moves);
                },
                child: Text("  " + moveParts[1].toString() + " ",
                    style: TextStyle(
                      color: _textColors[int.parse(moveParts[0].split("")[0]) - 1][0],
                      fontSize: 24,
                    )
                )
            ),
          )
      );

      if (moveParts.length == 3) {
        movesTextChildren.add(
            Container(
              decoration: BoxDecoration(
                border: _borders[int.parse(moveParts[0].split("")[0]) - 1][1],
                borderRadius: BorderRadius.circular(10),
                color: _colors[int.parse(moveParts[0].split("")[0]) - 1][1],
              ),

              child: InkWell(
                  onTap: () {
                    print("__MOVE PARTS___");
                    print(moveParts);
                    int index = int.parse(moveParts[0].split("")[0]);
                    print("___INDEX___ "+index.toString());
                    print("___STEPS___");
                    print(steps);
                    print("___MOVE TO INDEX___"+(index*2-indexToAttach).toString());
                    print("___indexToAttach___"+indexToAttach.toString());
                    index = int.parse(moveParts[0].split("")[0]);
                    String currFen = steps.split("|")[index*2-indexToAttach];


                    setState(() {
                      _fen = currFen;
                      _colors.forEach((row){
                        row[0] = Colors.transparent;
                        row[1] = Colors.transparent;
                      });

                      _borders.forEach((row){
                        row[0] = Border.all(color: Colors.transparent);
                        row[1] = Border.all(color: Colors.transparent);
                      });

                      _textColors.forEach((row){
                        row[0] = Colors.black;
                        row[1] = Colors.black;
                      });

                      _borders[int.parse(moveParts[0].split('')[0])-1][1] =  Border.all(color: Colors.black);
                      _colors[int.parse(moveParts[0].split('')[0])-1][1] =  darkGreen;
                      _textColors[int.parse(moveParts[0].split('')[0])-1][1] =  Colors.white;
                    });
                    this.movesTextChildren = updateMovesText(moves);
                  },
                  child: Text("  " + moveParts[2].toString() + " ",
                      style: TextStyle(
                        color: _textColors[int.parse(moveParts[0].split("")[0]) - 1][1],
                        fontSize: 24,
                      )
                  )
              ),
            )
        );
      }
    }
    return movesTextChildren;
  }
  Future<void> changeCommentaryDialog(int openingId, int attachToIndex, String side) async {
    Database db = await openDatabase(
        await getDatabasesPath() + VariantCommentaryContentFiller.dbName);
    await db.execute(VariantCommentaryContentFiller.onCreateString);

    DB dbCommentaryManipulator = DB(
        db,
        VariantCommentaryContentFiller.dbName,
        VariantCommentaryContentFiller.tableName,
        VariantCommentaryContentFiller.onCreateString
    );

    String commentary = " ";
    var fullcommentaryInfo = await dbCommentaryManipulator.queryCommentary(openingId, attachToIndex, side);
    commentary = fullcommentaryInfo.isNotEmpty ? fullcommentaryInfo[0]["commentaryText"] : "";

    print("commentary: $commentary");
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Комментарий к ходу'),
            content: SingleChildScrollView(
              child: ListBody(
                children:  <Widget>[
                  TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    initialValue: commentary,
                    decoration: const InputDecoration(
                      helperText: 'Комментарий',
                    ),
                    onChanged: (value) {
                      commentary = value;
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Отменить'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(
                  'Сохранить',
                  style: TextStyle(color: darkGreen),
                ),
                onPressed: () {
                  VariantCommentary newComment = VariantCommentary(
                      attachToIndex: attachToIndex,
                      side: side,
                      id: fullcommentaryInfo.isNotEmpty ? fullcommentaryInfo[0]['id'] :1,
                      commentaryText: commentary,
                      openingID: openingId
                  );
                  fullcommentaryInfo.isNotEmpty ?
                  dbCommentaryManipulator.update(newComment) :
                  dbCommentaryManipulator.insert(newComment);
                  setState(() {
                    //name = newName;
                    Navigator.of(context).pop();
                  });
                },
              ),
            ],
          );
        }
    );
  }

  Color? getSelectedFillColor(List<bool> isSelected) {
    Map<int, Color> colors = cbh.ChessboardHelper.importanceBackColors;

    int trueIndex= isSelected.indexOf(true);
    return colors[trueIndex];
  }
  Color? getSelectedTextColor(List<bool> isSelected) {
    Map<int, Color> colors = {
      0 : Colors.black,
      1 : Colors.black,
      2 : Colors.black,
    };
    int trueIndex= isSelected.indexOf(true);
    return colors[trueIndex];
  }

}
