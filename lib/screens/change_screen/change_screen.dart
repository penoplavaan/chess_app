import 'package:chess_app/ContentFillers/invariant_opening_content_fliier.dart';
import 'package:chess_app/ContentFillers/move_commentary_content_fliier.dart';
import 'package:chess_app/ContentFillers/opening_variants_content_fliier.dart';
import 'package:chess_app/Models/move_commentary.dart';
import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Models/variant_of_opening.dart';
import 'package:chess_app/Services/db.dart';
import 'package:chess_app/screens/main_screen/main_screen.dart';
import 'package:chess_app/screens/new_variation_screen/add_variation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stateless_chessboard/flutter_stateless_chessboard.dart';
import 'package:http/http.dart';
import 'package:sqflite/sqflite.dart';
import 'package:chess_app/Helpers/chessboard_helper.dart' as cbh;
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import '../../utils.dart';

class ChangeOpeningScreen extends StatefulWidget {
  var moves;

  ChangeOpeningScreen(
    this.moves, {
    Key? key,
  }) : super(key: key);

  @override
  _ChangeOpeningScreenState createState() => _ChangeOpeningScreenState(moves);
}

class _ChangeOpeningScreenState extends State<ChangeOpeningScreen> {
  var queriedAttachedVariants = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!
        .addPostFrameCallback((_) => _scrollController.animateTo(
        _scrollController.position.maxScrollExtent+100,
        duration: Duration(milliseconds: 1500),
        curve: Curves.ease));


      Future.delayed(Duration.zero,() async {
        var variants = await queryAllVariants();
        if(!moves['attachedVariants'].toString().contains("|") && moves['attachedVariants'] != null){
         var currentVariantID =moves['attachedVariants'].toString();
         variants.forEach((allVariantsVariant){
           if(int.parse(currentVariantID) == allVariantsVariant['id']){
             //print(allVariantsVariant);
             setState(() {
               queriedAttachedVariants.add(
                   allVariantsVariant['attachToIndex'].toString()+  (allVariantsVariant['movementsTextual'].contains("...")? "b" : "w")
               );
             });
           }
         });
        }
        else{
          (moves['attachedVariants']).split("|").forEach((currentVariantID) async {
            variants.forEach((allVariantsVariant){
              if(int.parse(currentVariantID) == allVariantsVariant['id']){
                print(allVariantsVariant);
                setState(() {
                  queriedAttachedVariants.add(
                      allVariantsVariant['attachToIndex'].toString()+  (allVariantsVariant['movementsTextual'].contains("...")? "b" : "w")
                  );
                });
              }
            });
          });
        }
      }).whenComplete(() =>setState((){
        movesTextChildren = updateMovesText(stepsTextual);
        updateMovesText(stepsTextual);
        attachedVariantsChildren = [];
        moveIsSelected = false;
        print("COMPLETED!");
      }));
  }

  bool moveIsSelected = false;
  List<int> moveVariations = [];
  List<int> originalMoveVariations = [];

  String initialSide = 'white';
  String fenToStartVariation = "";
  int indexToAttach = 1;
  int attachToIndex = 0;

  int index = 0;
  String side = "w";


  var moves = {};
  String orientColor = "white";
  List fenArray = [];
  List<Container> rtc = [];

  String _fen = "";
  int moveIndex = 0;
  int errorCount = 0;
  int totalMovementsCount = 0;

  String stepsTextual = "";
  int fullMoveId = 1;
  List<Container> movesTextChildren = [];

  List<Widget> attachedVariantsChildren = [];

  int emptyFieldsCount = 0;

  Map<String, dynamic> orientation = {
    'white': BoardColor.WHITE,
    'black': BoardColor.BLACK
  };
  int pervClickedIndex = -1;
  String pervClickedSide ='';
  final darkGreen = const Color.fromRGBO(116, 150, 90, 1);
  final lightGreen = const Color.fromRGBO(235, 239, 214, 1);
  final backColor = const Color.fromRGBO(32, 36, 33, 1);
  final gregColor = const Color.fromRGBO(237, 237, 237, 1);
  final changeColor = const Color.fromRGBO(226, 133, 24, 1);
  final deleteColor = const Color.fromRGBO(226, 24, 24, 1);
  String steps = "";

  String name = "";
  String subName = "";

  ScrollController _scrollController = ScrollController();

  final _colors = cbh.ChessboardHelper.highliteMoveColors;

  final _borders = cbh.ChessboardHelper.highliteMoveBorders;

  final _textColors =  cbh.ChessboardHelper.highliteMoveTextColors;




  _ChangeOpeningScreenState(moves) {
    moveIndex = moves["movements"].split("|").length - 2;
    this.moves = moves;
    this.orientColor = "white";
    this.steps = this.moves["movements"];
    this.stepsTextual = this.moves["movementsTextual"];
    initialSide = this.moves["side"];
    for (int i = 0; i < moves["movements"].split("|").length; i++) {
      fenArray.add(moves["movements"].split("|")[i]);
    }
   // this.movesTextChildren = updateMovesText(this.stepsTextual);
    _fen = moves["movements"].split("|")[moveIndex];

    emptyFieldsCount = getCurrEmptyFieldsCount(_fen);

    if (this.moves["attachedVariants"] != ""){
      if(!this.moves["attachedVariants"].toString().contains("|")) {
        moveVariations.add(this.moves["attachedVariants"]);
      }
      else{
        this.moves["attachedVariants"].split("|").forEach((variant){
          moveVariations.add(int.parse(variant));
        });
      }
    }
    originalMoveVariations = moveVariations;
    name = moves["name"].toString();
    subName = moves["subName"].toString();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width;

    const addLeftPadding = 3;
    const addTopPadding = 3;
    final topLettersPadding = width/8*7+width/64*5;
    final leftDigitsPadding = width - width/32;
    Color lightLetterColor = lightGreen;
    Color darkLetterColor = darkGreen;
    int? dropDownValue = 2;

    return Scaffold(
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 30.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            OutlinedButton(
                style: OutlinedButton.styleFrom(
                  shape:  CircleBorder(),
                  padding: EdgeInsets.all(20),
                  backgroundColor: Colors.white,
                ),
                onPressed: () {
                  if(moveIsSelected){
                    addNewVariation(context);
                  }
                },

                child: Icon(Icons.add, color: moveIsSelected ? changeColor : Colors.grey,)),
            OutlinedButton(
                style: OutlinedButton.styleFrom(
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(20),
                  backgroundColor: Colors.white,
                ),
                onPressed: (){
                  if(steps.split('|').length != 2) {
                    moveIsSelected = false;
                    if (attachedVariantsChildren.isNotEmpty) {
                      attachedVariantsChildren = [];
                    }

                    _colors.forEach((row) {
                      row[0] = Colors.transparent;
                      row[1] = Colors.transparent;
                    });

                    _borders.forEach((row) {
                      row[0] = Border.all(color: Colors.transparent);
                      row[1] = Border.all(color: Colors.transparent);
                    });

                    _textColors.forEach((row) {
                      row[0] = Colors.black;
                      row[1] = Colors.black;
                    });

                    List<String> movesList = stepsTextual.split(" ");
                    List<String> lastMoveList =
                        movesList[stepsTextual.split(" ").length - 1]
                            .split(" ");

                    if (lastMoveList.length == 3) {
                      lastMoveList.removeLast();
                      movesList[stepsTextual.split(" ").length - 1] =
                          lastMoveList.join(" ");
                    } else {
                      movesList.removeLast();
                      lastMoveList = [];
                    }

                    stepsTextual = movesList.join(" ");
                    movesTextChildren = updateMovesText(stepsTextual);

                    moveIndex--;
                    var ts = steps.split("|");
                    ts.removeLast();
                    ts.removeLast();
                    steps = ts.join("|") + "|";
                    _fen = steps.split("|")[moveIndex];

                    emptyFieldsCount = getCurrEmptyFieldsCount(_fen);

                    List<int> indexes = [];
                    RegExp regExp = RegExp(
                      r"\d\.",
                    );
                    regExp.allMatches(stepsTextual).forEach((match) {
                      //print("stringMatch: ${match.group(0)!.split(".")[0]}");
                      indexes.add(int.parse(match.group(0)!.split(".")[0]));
                    });

                    moveVariations.forEach((moveVariantID) async {
                      var fullMoveVariantInfo =
                          (await queryCurrentVariant(moveVariantID))[0];
                      print(
                          "fullMoveVariantInfo: ${fullMoveVariantInfo["attachToIndex"]}");
                      var id = fullMoveVariantInfo["attachToIndex"];
                      print("id is: $id");
                      var lastMove = stepsTextual.split(" ").last;

                      if (!indexes.contains(id)) {
                        moveVariations.remove(moveVariantID);
                        print("moveVariations: $moveVariations");
                      }

                      if (lastMove.split(" ").length == 2 &&
                          id ==
                              int.parse(lastMove.split(" ")[0].split(".")[0]) &&
                          fullMoveVariantInfo["movementsTextual"]
                              .contains("...")) {
                        moveVariations.remove(moveVariantID);
                      }

                    });
                    setState(() {});
                  }
                },
                child: Icon(Icons.undo, color: Colors.black,)),
          ],
        ),
      ),
      appBar: AppBar(

        leading: TextButton(
          onPressed: () {
            showBackOrSaveDialog();
          },
          child: Icon(
            Icons.arrow_back_outlined,
            color: Colors.white,
          ),
        ),
        /*
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
          Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(name),//moves["name"].toString()),
            //Text(moves["subName"].toString(), style: TextStyle(fontSize: 15),),
            Text(subName,  style: TextStyle(fontSize: 15)),
          ],
        ),
            ElevatedButton(
              onPressed: ()   {
                 changeNameDialog();
              },
              child: Icon(Icons.arrow_drop_down_outlined),
              style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all(backColor),
              ),

            ),
            ElevatedButton(
              onPressed: () async {
                //await queryInvariantOpenings();
                if (_fen !=
                    "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1") {


                    String joinedVariants = moveVariations.join("|");

                    (changeOpening(name,subName, steps,
                        stepsTextual, moves["id"], joinedVariants));
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DismissibleApp())
                    );
                } else{  }
              },
              child: Icon(Icons.save),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.transparent)
              ),
            )
          ],
        ),
        */
        title: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(name),//moves["name"].toString()),
                    //Text(moves["subName"].toString(), style: TextStyle(fontSize: 15),),
                    Text(subName,  style: TextStyle(fontSize: 15)),
                  ],
                ),
                ElevatedButton(
                  onPressed: () async {
                    //await queryInvariantOpenings();
                    if (_fen !=
                        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1") {


                      String joinedVariants = moveVariations.join("|");

                      (changeOpening(name,subName, steps,
                          stepsTextual, moves["id"], joinedVariants));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DismissibleApp())
                      );
                    } else{  }
                  },
                  child: Icon(Icons.save),
                  style: ButtonStyle(
                      backgroundColor:
                      MaterialStateProperty.all(Colors.transparent)
                  ),
                )
              ],
            ),
            DropdownButton(
                borderRadius: BorderRadius.circular(10),
              underline: Container(),
              //iconSize: 0,
              value: dropDownValue,
              onChanged: (newVal){
            },
            items: [
              DropdownMenuItem(
                value: 0,
                child: SizedBox(
                  width: 150,
                  height: 70,
                  child: TextFormField(
                    initialValue: name,
                    decoration: const InputDecoration(
                      helperText: 'Название',
                    ),
                    onChanged: (value) {
                      name = value;
                    },
                  ),
                ),
              ),
              DropdownMenuItem(
                value: 1,
                child: SizedBox(
                  width: 150,
                  height: 70,
                  child: TextFormField(
                    initialValue: subName,
                    decoration: const InputDecoration(
                      helperText: 'Вариант/система',
                    ),
                    onChanged: (value) {
                      subName = value;
                    },
                  ),
                ),
              ),
              const DropdownMenuItem(
                value: 2,
                child: Text(''),
              ),
            ],
          ),]
        ),
        backgroundColor: backColor,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [

            Stack(
              children: [
                Chessboard(
                darkSquareColor: Color.fromRGBO(116, 150, 90, 1),
                lightSquareColor: Color.fromRGBO(235, 239, 214, 1),
                fen: _fen,
                orientation: initialSide == 'white' ? BoardColor.WHITE : BoardColor.BLACK,
                size: size.width,
                onMove: (move) {
                  if(!moveIsSelected) {
                    String moveFrom = move.from;
                    String moveTo = move.to;
                    String prevFen = _fen;
                    final nextFen = makeMove(_fen, {
                      'from': move.from,
                      'to': move.to,
                      'promotion': 'q',
                    });

                    if (nextFen != null) {
                      _scrollController.animateTo(
                          _scrollController.position.maxScrollExtent + 100,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.ease);
                      moveIndex++;
                      steps += nextFen + "|";
                      setState(() {
                        _fen = nextFen;
                        String currFen = _fen;
                        String figFromColor = prevFen.split(" ")[1];

                        int currEmptyFieldsCount = getCurrEmptyFieldsCount(
                            currFen);

                        String moveTextual = currEmptyFieldsCount ==
                            emptyFieldsCount
                            ? getMovedFigure(currFen.split(" ")[0], moveTo) +
                            moveTo
                            : validateMovedFigure(currFen, moveFrom, moveTo) +
                            "×" +
                            moveTo;


                        movesTextChildren =
                            updateMovesText(this.stepsTextual);

                        //getMovedFigure(curr_fen.split(" ")[0], moveTo) + moveTo;
                        emptyFieldsCount = currEmptyFieldsCount;
                        if (moveFrom == 'e1' &&
                            moveTo == 'c1' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♔') {
                          stepsTextual += " " +
                              ((moveIndex - moveIndex % 2) / 2 + 1)
                                  .round()
                                  .toString() +
                              ". " +
                              "0-0-0";

                          fullMoveId++;
                        } else if (moveFrom == 'e1' &&
                            moveTo == 'g1' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♔') {
                          stepsTextual += " " +
                              ((moveIndex - moveIndex % 2) / 2 + 1)
                                  .round()
                                  .toString() +
                              ". " +
                              "0-0";

                          fullMoveId++;
                        } else if (moveFrom == 'e8' &&
                            moveTo == 'c8' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♚') {
                          stepsTextual += " 0-0-0";
                        } else if (moveFrom == 'e8' &&
                            moveTo == 'g8' &&
                            getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                '♚') {
                          stepsTextual += " 0-0";
                        } else {
                          if (figFromColor == "w") {
                            stepsTextual += " " +
                                ((moveIndex - moveIndex % 2) / 2 + 1)
                                    .round()
                                    .toString() +
                                ". " +
                                moveTextual;

                            fullMoveId++;
                          } else {
                            stepsTextual += " " + moveTextual;
                          }
                        }
                        movesTextChildren =
                            updateMovesText(this.stepsTextual);
                      });
                    }
                  }
                  else{
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(SnackBar(content: Text('Сначала уберите выделение хода!')));
                  }
                }
              ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*0 + addLeftPadding
                  ),
                  child: Text("a", style: TextStyle(color: lightLetterColor),),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*1+ addLeftPadding
                  ),
                  child: Text("b", style: TextStyle(color: darkLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*2+ addLeftPadding
                  ),
                  child: Text("c", style: TextStyle(color: lightLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*3+ addLeftPadding
                  ),
                  child: Text("d", style: TextStyle(color: darkLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*4+ addLeftPadding
                  ),
                  child: Text("e", style: TextStyle(color: lightLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*5+ addLeftPadding
                  ),
                  child: Text("f", style: TextStyle(color: darkLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*6+ addLeftPadding
                  ),
                  child: Text("g", style: TextStyle(color: lightLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: topLettersPadding,
                      left: width/8*7+ addLeftPadding
                  ),
                  child: Text("h", style: TextStyle(color: darkLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*0,
                      left: leftDigitsPadding
                  ),
                  child: Text("8", style: TextStyle(color: lightLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*1,
                      left: leftDigitsPadding
                  ),
                  child: Text("7", style: TextStyle(color: darkLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*2,
                      left: leftDigitsPadding
                  ),
                  child: Text("6", style: TextStyle(color: lightLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*3,
                      left: leftDigitsPadding
                  ),
                  child: Text("5", style: TextStyle(color: darkLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*4,
                      left: leftDigitsPadding
                  ),
                  child: Text("4", style: TextStyle(color: lightLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*5,
                      left: leftDigitsPadding
                  ),
                  child: Text("3", style: TextStyle(color: darkLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*6,
                      left: leftDigitsPadding
                  ),
                  child: Text("2", style: TextStyle(color: lightLetterColor)),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: width/8*7,
                      left: leftDigitsPadding
                  ),
                  child: Text("1", style: TextStyle(color: darkLetterColor)),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromRGBO(235, 239, 214, 1),
                ),
                width: 450,
                child: SafeArea(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    controller: _scrollController,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 12, right: 12, bottom: 10
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: List<Container>.from(movesTextChildren),
                                ),
                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: size.width,
              height: 120,
              child: ListView(
                children: List<Widget>.from(attachedVariantsChildren),
              ),
            )

          ],
        ),
      ),
    );
  }

  Future queryInvariantOpenings() async {
    Database db = await openDatabase(
        await getDatabasesPath() + InvariantOpeningContentFiller.dbName);
    await db.execute(InvariantOpeningContentFiller.onCreateString);
    DB dbInvariantOpeningManipulator = DB(
        db,
        InvariantOpeningContentFiller.dbName,
        InvariantOpeningContentFiller.tableName,
        InvariantOpeningContentFiller.onCreateString);

    return dbInvariantOpeningManipulator.queryAll();
  }

  Future changeOpening(name, subName, steps, movementsTextual, id, attachedVariants) async {
    Response response = (await http.get(Uri.http('penoplavaan.space:3000', '/openings/$id')));
    if (response.statusCode == 200) {
      var opening =
      convert.jsonDecode(response.body) as Map<String, dynamic>;
      var side = opening["side"];
      print("side is: "+side);
      var url = Uri.http('penoplavaan.space:3000', '/openings/$id');

      await http.put(url, body: {
        'name': name,
        'subName': subName,
        'movements': steps,
        'movementsTextual': movementsTextual,
        'attachedVariants': attachedVariants,
        'side': side
      });
    }

  }

  String getMovedFigure(String curr_fen, String move_to) {
    List<String> digits = ['1', '2', '3', '4', '5', '6', '7', '8'];
    Map<String, int> mapToGetIndex = {
      "a": 0,
      "b": 1,
      "c": 2,
      "d": 3,
      "e": 4,
      "f": 5,
      "g": 6,
      "h": 7
    };

    Map<String, dynamic> mapNameSymbol = {
      "K": "♔",
      "Q": "♕",
      "R": "♖",
      "B": "♗",
      "N": "♘",
      "k": "♚",
      "q": "♛",
      "r": "♜",
      "b": "♝",
      "n": "♞",
    };

    List moveToList = [
      8 - int.parse(move_to.split('')[1]),
      mapToGetIndex[move_to.split('')[0]]
    ];
    var fieldList = curr_fen.split('/').map((e) {
      String ourString = e;
      String newString = "";

      for (int i = 0; i < e.length; i++) {
        if (digits.contains(e[i])) {
          newString += '1' * int.parse(ourString[i]);
        } else {
          newString += ourString[i];
        }
      }
      return newString.split('');
    }).toList();

    String valueToReturn = fieldList[moveToList[0]][moveToList[1]];
    return valueToReturn.toLowerCase() == 'p'
        ? ""
        : mapNameSymbol[valueToReturn];
  }

  int getCurrEmptyFieldsCount(String fen) {
    int currEmptyFieldsCount = 0;

    List fenElements = fen.split(" ")[0].split("/").join("").split("");
    fenElements.forEach((element) {
      if (isNumeric(element)) {
        currEmptyFieldsCount += int.parse(element);
      }
    });

    return currEmptyFieldsCount;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  List<Container> updateMovesText(moves) {

    List<Container> movesTextChildren = [];
    for (var i = 1; i < moves.split(" ").length; i++) {
      var moveParts = moves.split(" ")[i].split(" ");

      int currIndex = int.parse(moveParts[0].split("")[0]);
      //print("movePARTS: $moveParts");
     // print("movePARTS currIndex: ${currIndex}");


      movesTextChildren.add(
          Container(
            child: Text(" " + moveParts[0].toString() + " ",
                style: const TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                )
            ),
          )
      );
      //
      movesTextChildren.add(
          Container(
            decoration: BoxDecoration(
              border: _borders[int.parse(moveParts[0].split("")[0]) - 1][0],
              borderRadius: BorderRadius.circular(10),
              color: _colors[int.parse(moveParts[0].split("")[0]) - 1][0],
            ),

            child: InkWell(
              onLongPress: () async {

                int currIndex = int.parse(moveParts[0].split("")[0]);
                side = "w";

                await changeCommentaryDialog(this.moves["id"],currIndex, side);

              },
                onTap: () async {
                  if(moveIsSelected && pervClickedSide=="w" && pervClickedIndex == int.parse(moveParts[0].split("")[0])){
                    moveIsSelected = false;
                    attachedVariantsChildren = [];
                    setState(() {
                      _fen = steps.split("|")[steps.split("|").length-2];

                      _colors.forEach((row){
                        row[0] = Colors.transparent;
                        row[1] = Colors.transparent;
                      });

                      _borders.forEach((row){
                        row[0] = Border.all(color: Colors.transparent);
                        row[1] = Border.all(color: Colors.transparent);
                      });

                      _textColors.forEach((row){
                        row[0] = Colors.black;
                        row[1] = Colors.black;
                      });

                      this.movesTextChildren = updateMovesText(moves);
                    });

                  }
                  else {
                    index = int.parse(moveParts[0].split("")[0]);
                    String currFen = steps.split("|")[index * 2 - 1];

                    moveIsSelected = true;
                    attachToIndex = index * 2 - 2;

                    fenToStartVariation = steps.split("|")[index * 2 - 2];
                    side = "w";
                    pervClickedIndex = index;
                    pervClickedSide = side;
                    var attachedMoves =
                        await queryAttachedVariants(index, side);

                    attachedVariantsChildren = [];
                    attachedMoves.forEach((variant) {
                      createNewAttachedvariant(variant);
                    });

                    setState(() {
                      _fen = currFen;
                      indexToAttach = index;

                      _colors.forEach((row) {
                        row[0] = Colors.transparent;
                        row[1] = Colors.transparent;
                      });

                      _borders.forEach((row) {
                        row[0] = Border.all(color: Colors.transparent);
                        row[1] = Border.all(color: Colors.transparent);
                      });

                      _textColors.forEach((row) {
                        row[0] = Colors.black;
                        row[1] = Colors.black;
                      });

                      _borders[int.parse(moveParts[0].split('')[0]) - 1][0] =
                          Border.all(color: Colors.black);
                      _colors[int.parse(moveParts[0].split('')[0]) - 1][0] =
                          darkGreen;
                      _textColors[int.parse(moveParts[0].split('')[0]) - 1][0] =
                          Colors.white;
                    });

                    this.movesTextChildren = updateMovesText(moves);
              }
            },
                child: Text("  " + moveParts[1].toString() + " ",
                    style: TextStyle(
                      color: _textColors[int.parse(moveParts[0].split("")[0]) - 1][0],
                      fontSize: 24,
                      decoration: queriedAttachedVariants.contains(moveParts[0].split("")[0] + "w") ?  TextDecoration.underline : TextDecoration.none,
                    )
                )
            ),
          )
      );

      if (moveParts.length == 3) {
        movesTextChildren.add(
            Container(
              decoration: BoxDecoration(
                border: _borders[int.parse(moveParts[0].split("")[0]) - 1][1],
                borderRadius: BorderRadius.circular(10),
                color: _colors[int.parse(moveParts[0].split("")[0]) - 1][1],
              ),

              child: InkWell(
                  onLongPress: () async {

                    int currIndex = int.parse(moveParts[0].split("")[0]);
                    side = "b";

                    await changeCommentaryDialog(this.moves["id"],currIndex, side);
                  },
                  onTap: () async {
                    print("queriedAttachedVariants: $queriedAttachedVariants");
                    if(moveIsSelected && pervClickedSide=="b" && pervClickedIndex == int.parse(moveParts[0].split("")[0])){
                      moveIsSelected = false;
                      setState(() {
                        _fen = steps.split("|")[steps.split("|").length-2];

                        _colors.forEach((row){
                          row[0] = Colors.transparent;
                          row[1] = Colors.transparent;
                        });

                        _borders.forEach((row){
                          row[0] = Border.all(color: Colors.transparent);
                          row[1] = Border.all(color: Colors.transparent);
                        });

                        _textColors.forEach((row){
                          row[0] = Colors.black;
                          row[1] = Colors.black;
                        });

                        this.movesTextChildren = updateMovesText(moves);
                      });

                    }
                    else {
                      moveIsSelected = true;

                      index = int.parse(moveParts[0].split("")[0]);

                      print('pervClickedIndex: $pervClickedIndex');
                      String currFen = steps.split("|")[index * 2];

                      attachToIndex = index * 2 - 1;
                      fenToStartVariation = steps.split("|")[index * 2 - 1];
                      side = "b";

                      pervClickedIndex = int.parse(moveParts[0].split("")[0]);
                      pervClickedSide =side;

                      var attachedMoves = await queryAttachedVariants(index, side);
                      attachedVariantsChildren = [];
                      attachedMoves.forEach((variant) {
                        createNewAttachedvariant(variant);
                      });

                      setState(() {
                        _fen = currFen;
                        indexToAttach = index;
                        _colors.forEach((row) {
                          row[0] = Colors.transparent;
                          row[1] = Colors.transparent;
                        });

                        _borders.forEach((row) {
                          row[0] = Border.all(color: Colors.transparent);
                          row[1] = Border.all(color: Colors.transparent);
                        });

                        _textColors.forEach((row) {
                          row[0] = Colors.black;
                          row[1] = Colors.black;
                        });

                        _borders[int.parse(moveParts[0].split('')[0]) - 1][1] =
                            Border.all(color: Colors.black);
                        _colors[int.parse(moveParts[0].split('')[0]) - 1][1] =
                            darkGreen;
                        _textColors[int.parse(moveParts[0].split('')[0]) - 1][1] =
                            Colors.white;
                      });
                      this.movesTextChildren = updateMovesText(moves);
                }
              },
                  child: Text("  " + moveParts[2].toString() + " ",
                      style: TextStyle(
                        color: _textColors[int.parse(moveParts[0].split("")[0]) - 1][1],
                        fontSize: 24,
                        decoration: queriedAttachedVariants.contains(moveParts[0].split("")[0] + "b") ?  TextDecoration.underline : TextDecoration.none,

                      )
                  )
              ),
            )
        );
      }
    }
    return movesTextChildren;
  }

  queryAttachedVariants(int attachToIndex, String side) async {
    //print("black "+ moveVariations.toString());
    //print("attachToIndex " + attachToIndex.toString());
    Database db = await openDatabase(
        await getDatabasesPath() + VariantOfOpeningContentFiller.dbName);
    await db.execute(VariantOfOpeningContentFiller.onCreateString);

    DB dbVariantOfOpeningManipulator = DB(
      db,
      VariantOfOpeningContentFiller.dbName,
      VariantOfOpeningContentFiller.tableName,
      VariantOfOpeningContentFiller.onCreateString
    );

    List<Map<String, dynamic>> queriedMovementsTextual = [];
    var variants =await dbVariantOfOpeningManipulator.queryAll();
    variants.forEach((variant) {
      if(moveVariations.contains( variant['id']) && variant['attachToIndex'] == attachToIndex){
        //print(variant);
        if (side == "b" && variant["movementsTextual"].indexOf("...") != -1){
          queriedMovementsTextual.add(variant);
        }
        else if (side == "w" && variant["movementsTextual"].indexOf("...") == -1){
          queriedMovementsTextual.add(variant);
        }
      }
    });

    return queriedMovementsTextual;
  }

  queryAllVariants() async{
    Database db = await openDatabase(
        await getDatabasesPath() + VariantOfOpeningContentFiller.dbName);
    await db.execute(VariantOfOpeningContentFiller.onCreateString);

    DB dbVariantOfOpeningManipulator = DB(
        db,
        VariantOfOpeningContentFiller.dbName,
        VariantOfOpeningContentFiller.tableName,
        VariantOfOpeningContentFiller.onCreateString
    );

    List<Map<String, dynamic>> queriedMovementsTextual = [];
    var variants =await dbVariantOfOpeningManipulator.queryAll();

    return variants;
  }

  queryCurrentVariant(int id) async{
    Database db = await openDatabase(
        await getDatabasesPath() + VariantOfOpeningContentFiller.dbName);
    await db.execute(VariantOfOpeningContentFiller.onCreateString);

    DB dbVariantOfOpeningManipulator = DB(
        db,
        VariantOfOpeningContentFiller.dbName,
        VariantOfOpeningContentFiller.tableName,
        VariantOfOpeningContentFiller.onCreateString
    );

    var variant =await dbVariantOfOpeningManipulator.query(id);

    return variant;
  }


  String validateMovedFigure(curr_fen, moveFrom, moveTo) {
    return getMovedFigure(curr_fen.split(" ")[0], moveTo) == ""
        ? moveFrom.split("")[0]
        : getMovedFigure(curr_fen.split(" ")[0], moveTo);
  }

  void addNewVariation(BuildContext context) async{
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>  AddVariationPage(indexToAttach, fenToStartVariation, attachToIndex,initialSide == 'white' ? BoardColor.WHITE : BoardColor.BLACK,
      )),
    );
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text("Добавлен новый вариант")));
    moveVariations.add(result);

    print("index " + index.toString());
    print("side " + side);
    var attachedMoves =  await queryAttachedVariants(index, side);
    print("attachedMoves: $attachedMoves");

    setState(() {
      queriedAttachedVariants.add(index.toString()+side);
      attachedVariantsChildren = [];
      attachedMoves.forEach((variant){
        createNewAttachedvariant(variant);
        print("vArIaNt" + variant.toString());
      });

      movesTextChildren = updateMovesText(stepsTextual);
    });

  }

  void createNewAttachedvariant(variant) {
    attachedVariantsChildren.add(
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0),
              child: TextButton(
                style: TextButton.styleFrom(
                    primary: Colors.black,
                  textStyle: const TextStyle(
                      fontSize: 15,
                  ),
                ),
                onPressed: () {},
                onLongPress: (){
                  showBackOrDeleteAttachedVariantDialog(variant["id"]);
                },
                child: Text(
                  variant["movementsTextual"].length >= 45 ?   variant["movementsTextual"].substring(0,45)+"...." : variant["movementsTextual"],
                  style: TextStyle(
                    color: cbh.ChessboardHelper.importanceBackColors[variant["importance"]]
                  ),
                ),
              ),
            ),
            Divider(
                color: Colors.black
            ),
          ],
        )
    );
  }


  Future<void> showBackOrSaveDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Внимание!'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const <Widget>[
                  Text('Все внесённые изменения будут удалены.'),
                  Text('Точно хотите выйти?'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Отмена'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: const Text(
                  'Выйти',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DismissibleApp()),
                  );
                },
              ),
            ],
          );
        }
    );
  }

  Future<void> showBackOrDeleteAttachedVariantDialog(int removeID) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Удалить вариант?'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const <Widget>[
                  Text(''),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Отмена'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: const Text(
                  'Удалить',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () async {
                  moveVariations.remove(removeID);

                  var attachedMoves =  await queryAttachedVariants(index, side);
                  setState(() {
                    attachedVariantsChildren = [];
                    attachedMoves.forEach((variant){
                      createNewAttachedvariant(variant);
                      print("vArIaNt" + variant.toString());
                    });

                    movesTextChildren = updateMovesText(stepsTextual);
                    Navigator.of(context).pop();
                  });
                },
              ),
            ],
          );
        }
    );
  }

  Future<void> changeNameDialog() async {
    String newName = name;
    String newSubName = subName;
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Изменение названия'),
            content: SingleChildScrollView(
              child: ListBody(
                children:  <Widget>[
                  TextFormField(
                    initialValue: name,
                    decoration: const InputDecoration(
                      helperText: 'Название',
                    ),
                    onChanged: (value) {
                      newName = value;
                    },
                  ),
                  TextFormField(
                    initialValue: subName,
                    decoration: const InputDecoration(
                      helperText: 'Вариант/система',
                    ),
                    onChanged: (value) {
                      newSubName = value;
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Отменить'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(
                  'Сохранить',
                  style: TextStyle(color: darkGreen),
                ),
                onPressed: () {
                  setState(() {
                    name = newName;
                    subName = newSubName;
                    Navigator.of(context).pop();
                  });
                },
              ),
            ],
          );
        }
    );
  }

  Future<void> changeCommentaryDialog(int openingId, int attachToIndex, String side) async {
    Database db = await openDatabase(
        await getDatabasesPath() + CommentaryContentFiller.dbName);
    await db.execute(CommentaryContentFiller.onCreateString);

    DB dbCommentaryManipulator = DB(
        db,
        CommentaryContentFiller.dbName,
        CommentaryContentFiller.tableName,
        CommentaryContentFiller.onCreateString
    );

    String commentary = " ";
    var fullcommentaryInfo = await dbCommentaryManipulator.queryCommentary(openingId, attachToIndex, side);
    commentary = fullcommentaryInfo.isNotEmpty ? fullcommentaryInfo[0]["commentaryText"] : "";

    print("commentary: $commentary");
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Комментарий к ходу'),
            content: SingleChildScrollView(
              child: ListBody(
                children:  <Widget>[
                  TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    initialValue: commentary,
                    decoration: const InputDecoration(
                      helperText: 'Комментарий',
                    ),
                    onChanged: (value) {
                      commentary = value;
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Отменить'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(
                  'Сохранить',
                  style: TextStyle(color: darkGreen),
                ),
                onPressed: () {
                  Commentary newComment = Commentary(
                      attachToIndex: attachToIndex,
                      side: side,
                      id: fullcommentaryInfo.isNotEmpty ? fullcommentaryInfo[0]['id'] :1,
                      commentaryText: commentary,
                      openingID: openingId
                  );
                  fullcommentaryInfo.isNotEmpty ?
                    dbCommentaryManipulator.update(newComment) :
                    dbCommentaryManipulator.insert(newComment);
                  setState(() {
                    //name = newName;
                    Navigator.of(context).pop();
                  });
                },
              ),
            ],
          );
        }
    );
  }

  getQueriedAttachedVariants(index, side) async {
    var attachedMoves =  await queryAttachedVariants(index, side);
    //print("attachedMoves moves from function: $attachedMoves");
    var variants = [];
    setState(() {
      if( attachedMoves.length > 0){
        variants.add(index);
      }
    });
    return variants;
    //print("queriedAttachedVariants moves from function: $queriedAttachedVariants");
  }
}

