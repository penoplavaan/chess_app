import 'dart:convert' as convert;

import 'package:chess_app/ContentFillers/invariant_opening_content_fliier.dart';
import 'package:chess_app/ContentFillers/user_content_filler.dart';
import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Services/db.dart';
import 'package:chess_app/screens/main_screen/main_screen.dart';
import 'package:chess_app/screens/new_variation_screen/add_variation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_stateless_chessboard/flutter_stateless_chessboard.dart';
import 'package:http/http.dart' as http;
import 'package:sqflite/sqflite.dart';

import '../../utils.dart';

class AddOpeningPage extends StatefulWidget {
  String fen = "";

  @override
  _AddOpeningPageState createState() => _AddOpeningPageState();
}

class _AddOpeningPageState extends State<AddOpeningPage>
    with AutomaticKeepAliveClientMixin<AddOpeningPage> {
  _AddOpeningPageState() {
    this.prevFen = _fen;
  }

  void initState() {
    super.initState();
    WidgetsBinding.instance!
        .addPostFrameCallback((_) => showChooseSideScreen());
  }

  List<int> moveVariations = [];
  String _fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
  final darkGreen = const Color.fromRGBO(116, 150, 90, 1);
  final lightGreen = const Color.fromRGBO(235, 239, 214, 1);
  final backColor = const Color.fromRGBO(32, 36, 33, 1);
  final gregColor = const Color.fromRGBO(237, 237, 237, 1);
  final changeColor = const Color.fromRGBO(226, 133, 24, 1);
  final deleteColor = const Color.fromRGBO(226, 24, 24, 1);
  String steps = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1|";
  String figures = "rnbqkpRNBQKP";

  String prevFen = "";
  String newOpeningName = "Название";
  String newOpeningSubName = "...";
  String stepsTextual = "";
  int fullMoveId = 1;
  List<Container> movesTextChildren = [];

  String side = "white";
  Map<String, dynamic> orientMap = {
    "white": BoardColor.WHITE,
    "black": BoardColor.BLACK,
  };
  String fenToStartVariation = "";
  int indexToAttach = 1;
  int attachToIndex = 0;

  ScrollController _scrollController = ScrollController();

  int pervClickedIndex = -1;
  String pervClickedSide = '';

  var _colors = [
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
    [Colors.transparent, Colors.transparent],
  ];

  var _borders = [
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
    [
      Border.all(color: Colors.transparent),
      Border.all(color: Colors.transparent)
    ],
  ];

  var _textColors = [
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
    [Colors.black, Colors.black],
  ];

  bool moveIsSelected = false;

  int emptyFieldsCount = 32;
  int moveIndex = 0;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width;

    const addLeftPadding = 0;
    final topLettersPadding = width / 8 * 7 + width / 64 * 5;
    final leftDigitsPadding = width - width / 32;
    Color lightLetterColor = lightGreen;
    Color darkLetterColor = darkGreen;

    //top, left
    List whitePositionsArray = [
      //letters
      [topLettersPadding, width / 8 * 0 + addLeftPadding],
      [topLettersPadding, width / 8 * 1 + addLeftPadding],
      [topLettersPadding, width / 8 * 2 + addLeftPadding],
      [topLettersPadding, width / 8 * 3 + addLeftPadding],
      [topLettersPadding, width / 8 * 4 + addLeftPadding],
      [topLettersPadding, width / 8 * 5 + addLeftPadding],
      [topLettersPadding, width / 8 * 6 + addLeftPadding],
      [topLettersPadding, width / 8 * 7 + addLeftPadding],
      //digits
      [width / 8 * 0, leftDigitsPadding],
      [width / 8 * 1, leftDigitsPadding],
      [width / 8 * 2, leftDigitsPadding],
      [width / 8 * 3, leftDigitsPadding],
      [width / 8 * 4, leftDigitsPadding],
      [width / 8 * 5, leftDigitsPadding],
      [width / 8 * 6, leftDigitsPadding],
      [width / 8 * 7, leftDigitsPadding],
    ];

    //top, left
    List blackPositionsArray = [
      //letters
      [width / 8 * 8 - width / 25, width / 8 * 8 - width / 40],
      [width / 8 * 8 - width / 25, width / 8 * 7 - width / 40],
      [width / 8 * 8 - width / 25, width / 8 * 6 - width / 40],
      [width / 8 * 8 - width / 25, width / 8 * 5 - width / 40],
      [width / 8 * 8 - width / 25, width / 8 * 4 - width / 40],
      [width / 8 * 8 - width / 25, width / 8 * 3 - width / 40],
      [width / 8 * 8 - width / 25, width / 8 * 2 - width / 40],
      [width / 8 * 8 - width / 25, width / 8 * 1 - width / 40],
      //digits
      [width / 8 * 8 - width / 25, 0.0],
      [width / 8 * 7 - width / 25, 0.0],
      [width / 8 * 6 - width / 25, 0.0],
      [width / 8 * 5 - width / 25, 0.0],
      [width / 8 * 4 - width / 25, 0.0],
      [width / 8 * 3 - width / 25, 0.0],
      [width / 8 * 2 - width / 25, 0.0],
      [width / 8 * 1 - width / 25, 0.0],
    ];
    int dropDownValue = 2;

    return Scaffold(
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 30.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            OutlinedButton(
                style: OutlinedButton.styleFrom(
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(20),
                  backgroundColor: Colors.white,
                ),
                onPressed: () {
                  if (moveIsSelected) {
                    addNewVariation(context);
                  }
                },
                child: Icon(
                  Icons.add,
                  color: moveIsSelected ? changeColor : Colors.grey,
                )),
            OutlinedButton(
                style: OutlinedButton.styleFrom(
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(20),
                  backgroundColor: Colors.white,
                ),
                onPressed: () {
                  if (steps.split('|').length != 2) {
                    setState(() {
                      List<String> movesList = stepsTextual.split(" ");
                      List<String> lastMoveList =
                          movesList[stepsTextual.split(" ").length - 1]
                              .split(" ");

                      if (lastMoveList.length == 3) {
                        lastMoveList.removeLast();
                        movesList[stepsTextual.split(" ").length - 1] =
                            lastMoveList.join(" ");
                      } else {
                        movesList.removeLast();
                        lastMoveList = [];
                      }

                      stepsTextual = movesList.join(" ");
                      movesTextChildren = updateMovesText(stepsTextual);

                      moveIndex--;
                      print("moveIndex: $moveIndex");
                      print("steps: " + steps);

                      print("steps.length: ${steps.split('|').length}");

                      var ts = steps.split("|");
                      ts.removeLast();
                      ts.removeLast();
                      steps = ts.join("|") + "|";
                      _fen = steps.split("|")[moveIndex];

                      print("steps: " + steps);
                      print("_fen: " + _fen);

                      emptyFieldsCount = getCurrEmptyFieldsCount(_fen);
                    });
                  } else {
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(SnackBar(
                          content:
                              Text("Невозможно отменить. Это первый ход!")));
                  }
                },
                child: Icon(
                  Icons.undo,
                  color: Colors.black,
                )),
          ],
        ),
      ),
      appBar: AppBar(
        title: Stack(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(newOpeningName), //moves["name"].toString()),
                  //Text(moves["subName"].toString(), style: TextStyle(fontSize: 15),),
                  Text(newOpeningSubName, style: TextStyle(fontSize: 15)),
                ],
              ),
              ElevatedButton(
                onPressed: () async {
                  //await queryInvariantOpenings();
                  if (newOpeningName != "Название" &&
                      steps !=
                          "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1") {
                    print("joined moveVariations: " + moveVariations.join("|"));
                    await (addNewOpening(newOpeningName, newOpeningSubName,
                        steps, stepsTextual, moveVariations.join("|")));
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DismissibleApp()));
                  } else {
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(SnackBar(
                          content: Text(
                              "Вы не ввели название или не сделали ни одного хода!")));
                  }
                },
                child: Icon(Icons.save),
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.transparent)),
              )
            ],
          ),
          DropdownButton(
            borderRadius: BorderRadius.circular(10),
            underline: Container(),
            //iconSize: 0,
            value: dropDownValue,
            onChanged: (newVal) {},
            items: [
              DropdownMenuItem(
                value: 0,
                child: SizedBox(
                  width: 150,
                  height: 70,
                  child: TextFormField(
                    initialValue:
                        newOpeningName == "Название" ? "" : newOpeningName,
                    decoration: const InputDecoration(
                      helperText: 'Название',
                    ),
                    onChanged: (value) {
                      newOpeningName = value;
                    },
                  ),
                ),
              ),
              DropdownMenuItem(
                value: 1,
                child: SizedBox(
                  width: 150,
                  height: 70,
                  child: TextFormField(
                    initialValue:
                        newOpeningSubName == "..." ? "" : newOpeningSubName,
                    decoration: const InputDecoration(
                      helperText: 'Вариант/система',
                    ),
                    onChanged: (value) {
                      newOpeningSubName = value;
                    },
                  ),
                ),
              ),
              const DropdownMenuItem(
                value: 2,
                child: Text(''),
              ),
            ],
          ),
        ]),
        backgroundColor: backColor,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Stack(children: [
              Chessboard(
                  darkSquareColor: Color.fromRGBO(116, 150, 90, 1),
                  lightSquareColor: Color.fromRGBO(235, 239, 214, 1),
                  fen: _fen,
                  size: size.width,
                  orientation: orientMap[side],
                  onMove: (move) {
                    SystemSound.play(SystemSoundType.click);
                    if (!moveIsSelected) {
                      _scrollController.animateTo(
                          _scrollController.position.maxScrollExtent + 100,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.ease);
                      //print(move);
                      String moveFrom = move.from;
                      String moveTo = move.to;
                      prevFen = _fen;
                      final nextFen = makeMove(_fen, {
                        'from': move.from,
                        'to': move.to,
                        'promotion': 'q',
                      });

                      if (nextFen != null) {
                        moveIndex++;
                        steps += nextFen + "|";
                        setState(() {
                          _fen = nextFen;
                          String currFen = _fen;
                          //print(curr_fen);
                          String figFromColor = prevFen.split(" ")[1];

                          int currEmptyFieldsCount =
                              getCurrEmptyFieldsCount(currFen);

                          var checkedMoveTo = moveTo;
                          List lettersList = [
                            'a',
                            'b',
                            'c',
                            'd',
                            'e',
                            'f',
                            'g',
                            'h'
                          ];
                          print("[][][][]checkedMoveTo" + checkedMoveTo);

                          /*
                      if (side == "black"){
                        List checkedMoveToList = checkedMoveTo.split("");
                        checkedMoveToList[0] = lettersList[7-lettersList.indexOf(checkedMoveToList[0])];
                        checkedMoveToList[1] = (9 - int.parse(checkedMoveToList[1])).toString();
                        checkedMoveTo = checkedMoveToList.join("");
                      }*/

                          String moveTextual = currEmptyFieldsCount ==
                                  emptyFieldsCount
                              ? getMovedFigure(currFen.split(" ")[0], moveTo) +
                                  checkedMoveTo
                              : validateMovedFigure(currFen, moveFrom, moveTo) +
                                  "×" +
                                  checkedMoveTo;
                          movesTextChildren =
                              updateMovesText(this.stepsTextual);

                          //print('MOVE TEXTUAL: $moveTextual');
                          //print("processed: ${lettersList[lettersList.reversed.toList().indexOf(moveTextual.split('')[0])]}");
                          //print('SIDE: $side');

                          //getMovedFigure(curr_fen.split(" ")[0], moveTo) + moveTo;
                          emptyFieldsCount = currEmptyFieldsCount;
                          if (moveFrom == 'e1' &&
                              moveTo == 'c1' &&
                              getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                  '♔') {
                            //print('Рокировка 0-0-0');
                            stepsTextual += " " +
                                ((moveIndex - moveIndex % 2) / 2 + 1)
                                    .round()
                                    .toString() +
                                ". " +
                                "0-0-0";

                            fullMoveId++;
                          } else if (moveFrom == 'e1' &&
                              moveTo == 'g1' &&
                              getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                  '♔') {
                            //print('Рокировка 0-0');
                            stepsTextual += " " +
                                ((moveIndex - moveIndex % 2) / 2 + 1)
                                    .round()
                                    .toString() +
                                ". " +
                                "0-0";
                            fullMoveId++;
                          } else if (moveFrom == 'e8' &&
                              moveTo == 'c8' &&
                              getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                  '♚') {
                            //print('Рокировка 0-0-0');
                            stepsTextual += " 0-0-0";
                          } else if (moveFrom == 'e8' &&
                              moveTo == 'g8' &&
                              getMovedFigure(currFen.split(" ")[0], moveTo) ==
                                  '♚') {
                            //print('Рокировка 0-0');
                            stepsTextual += " 0-0";
                          } else {
                            if (figFromColor == "w") {
                              stepsTextual += " " +
                                  ((moveIndex - moveIndex % 2) / 2 + 1)
                                      .round()
                                      .toString() +
                                  ". " +
                                  moveTextual;

                              fullMoveId++;
                            } else {
                              stepsTextual += " " + moveTextual;
                            }
                          }
                          movesTextChildren =
                              updateMovesText(this.stepsTextual);
                        });
                      }
                    } else {
                      ScaffoldMessenger.of(context)
                        ..removeCurrentSnackBar()
                        ..showSnackBar(SnackBar(
                            content: Text('Сначала уберите выделение хода!')));
                    }
                  }),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[0][0]
                        : blackPositionsArray[0][0],
                    left: side == "white"
                        ? whitePositionsArray[0][1]
                        : blackPositionsArray[0][1]),
                child: Text(
                  "a",
                  style: TextStyle(
                      color:
                          side == "white" ? lightLetterColor : darkLetterColor),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[1][0]
                        : blackPositionsArray[1][0],
                    left: side == "white"
                        ? whitePositionsArray[1][1]
                        : blackPositionsArray[1][1]),
                child: Text("b",
                    style: TextStyle(
                        color: side == "white"
                            ? darkLetterColor
                            : lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[2][0]
                        : blackPositionsArray[2][0],
                    left: side == "white"
                        ? whitePositionsArray[2][1]
                        : blackPositionsArray[2][1]),
                child: Text("c",
                    style: TextStyle(
                        color: side == "white"
                            ? lightLetterColor
                            : darkLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[3][0]
                        : blackPositionsArray[3][0],
                    left: side == "white"
                        ? whitePositionsArray[3][1]
                        : blackPositionsArray[3][1]),
                child: Text("d",
                    style: TextStyle(
                        color: side == "white"
                            ? darkLetterColor
                            : lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[4][0]
                        : blackPositionsArray[4][0],
                    left: side == "white"
                        ? whitePositionsArray[4][1]
                        : blackPositionsArray[4][1]),
                child: Text("e",
                    style: TextStyle(
                        color: side == "white"
                            ? lightLetterColor
                            : darkLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[5][0]
                        : blackPositionsArray[5][0],
                    left: side == "white"
                        ? whitePositionsArray[5][1]
                        : blackPositionsArray[5][1]),
                child: Text("f",
                    style: TextStyle(
                        color: side == "white"
                            ? darkLetterColor
                            : lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[6][0]
                        : blackPositionsArray[6][0],
                    left: side == "white"
                        ? whitePositionsArray[6][1]
                        : blackPositionsArray[6][1]),
                child: Text("g",
                    style: TextStyle(
                        color: side == "white"
                            ? lightLetterColor
                            : darkLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[7][0]
                        : blackPositionsArray[7][0],
                    left: side == "white"
                        ? whitePositionsArray[7][1]
                        : blackPositionsArray[7][1]),
                child: Text("h",
                    style: TextStyle(
                        color: side == "white"
                            ? darkLetterColor
                            : lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[8][0]
                        : blackPositionsArray[8][0],
                    left: side == "white"
                        ? whitePositionsArray[8][1]
                        : blackPositionsArray[8][1]),
                child: Text("8", style: TextStyle(color: lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[9][0]
                        : blackPositionsArray[9][0],
                    left: side == "white"
                        ? whitePositionsArray[9][1]
                        : blackPositionsArray[9][1]),
                child: Text("7", style: TextStyle(color: darkLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[10][0]
                        : blackPositionsArray[10][0],
                    left: side == "white"
                        ? whitePositionsArray[10][1]
                        : blackPositionsArray[10][1]),
                child: Text("6", style: TextStyle(color: lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[11][0]
                        : blackPositionsArray[11][0],
                    left: side == "white"
                        ? whitePositionsArray[11][1]
                        : blackPositionsArray[11][1]),
                child: Text("5", style: TextStyle(color: darkLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[12][0]
                        : blackPositionsArray[12][0],
                    left: side == "white"
                        ? whitePositionsArray[12][1]
                        : blackPositionsArray[12][1]),
                child: Text("4", style: TextStyle(color: lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[13][0]
                        : blackPositionsArray[13][0],
                    left: side == "white"
                        ? whitePositionsArray[13][1]
                        : blackPositionsArray[13][1]),
                child: Text("3", style: TextStyle(color: darkLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[14][0]
                        : blackPositionsArray[14][0],
                    left: side == "white"
                        ? whitePositionsArray[14][1]
                        : blackPositionsArray[14][1]),
                child: Text("2", style: TextStyle(color: lightLetterColor)),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: side == "white"
                        ? whitePositionsArray[15][0]
                        : blackPositionsArray[15][0],
                    left: side == "white"
                        ? whitePositionsArray[15][1]
                        : blackPositionsArray[15][1]),
                child: Text("1", style: TextStyle(color: darkLetterColor)),
              ),
            ]),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromRGBO(235, 239, 214, 1),
                ),
                width: 450,
                child: SafeArea(
                  child: SingleChildScrollView(
                    controller: _scrollController,
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 12, right: 12, bottom: 10),
                            child: Container(
                              color: Color.fromRGBO(235, 239, 214, 1),
                              //child: Text(stepsTextual,style: TextStyle(fontSize: 22))
                              child: Container(
                                  //child: Text(stepsTextual,style: TextStyle(fontSize: 22))
                                  child: Row(
                                children:
                                    List<Container>.from(movesTextChildren),
                              )),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void addNewVariation(BuildContext context) async {
    BoardColor orientation =
        side == 'black' ? BoardColor.BLACK : BoardColor.WHITE;
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AddVariationPage(
              indexToAttach, fenToStartVariation, attachToIndex, orientation)),
    );
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(result.toString())));
    moveVariations.add(result);
    print(moveVariations.toString());
  }

  Future queryInvariantOpenings() async {
    Database db = await openDatabase(
        await getDatabasesPath() + InvariantOpeningContentFiller.dbName);
    await db.execute(InvariantOpeningContentFiller.onCreateString);
    DB dbInvariantOpeningManipulator = DB(
        db,
        InvariantOpeningContentFiller.dbName,
        InvariantOpeningContentFiller.tableName,
        InvariantOpeningContentFiller.onCreateString);

    return dbInvariantOpeningManipulator.queryAll();
  }

  Future addNewOpening(
      name, subName, steps, movementsTextual, attachedVariants) async {
    Database db = await openDatabase(
        await getDatabasesPath() + InvariantOpeningContentFiller.dbName);
    await db.execute(InvariantOpeningContentFiller.onCreateString);
    DB dbInvariantOpeningManipulator = DB(
        db,
        InvariantOpeningContentFiller.dbName,
        InvariantOpeningContentFiller.tableName,
        InvariantOpeningContentFiller.onCreateString);

    dbInvariantOpeningManipulator.insert(InvariantOpening(
      id: 1,
      name: name,
      subName: subName,
      movements: steps,
      movementsTextual: movementsTextual,
      attachedVariants: attachedVariants,
      side: side,
    ));

    Database dbUser =
        await openDatabase(await getDatabasesPath() + UserContentFiller.dbName);
    await db.execute(UserContentFiller.onCreateString);
    DB dbUserManipulator = DB(db, UserContentFiller.dbName,
        UserContentFiller.tableName, UserContentFiller.onCreateString);

    var userId = (await dbUserManipulator.query(1))[0]['externalId'].toString();
    var url = Uri.http('penoplavaan.space:3000', '/openings');

    var response = await http.post(url, body: {
      'name': name,
      'subName': subName,
      'movements': steps,
      'movementsTextual': movementsTextual,
      'attachedVariants': attachedVariants,
      'side': side,
      'userId': userId
    });
    if (response.statusCode == 201) {
      var jsonResponse =
          convert.jsonDecode(response.body) as Map<String, dynamic>;

      print(jsonResponse);
    } else {
      print(
          'Request failed with status: ${response.statusCode}  ${response.body}.');
    }
  }

  String getMovedFigure(String currFen, String moveTo) {
    List<String> digits = ['1', '2', '3', '4', '5', '6', '7', '8'];
    print("moveTo" + moveTo);

    var blackMoveTo = moveTo;
    if (side == "black") {
      List blackMoveToParts = blackMoveTo.split("");
      blackMoveToParts[1] = (9 - int.parse(blackMoveToParts[1])).toString();

      blackMoveTo = blackMoveToParts.join("");
      print("blackMoveTo" + blackMoveTo);
    }

    Map<String, dynamic> mapToGetIndex = {
      "a": 0,
      "b": 1,
      "c": 2,
      "d": 3,
      "e": 4,
      "f": 5,
      "g": 6,
      "h": 7
    };

    Map<String, dynamic> mapNameSymbol = {
      "K": "♔",
      "Q": "♕",
      "R": "♖",
      "B": "♗",
      "N": "♘",
      "k": "♚",
      "q": "♛",
      "r": "♜",
      "b": "♝",
      "n": "♞",
    };

    List<int> moveToList = [
      8 - int.parse(moveTo.split('')[1]),
      mapToGetIndex[moveTo.split('')[0]]
    ];
    print("moveTo" + moveTo.toString());
    var fieldList = currFen.split('/').map((e) {
      String ourString = e;
      String newString = "";

      for (int i = 0; i < e.length; i++) {
        if (digits.contains(e[i])) {
          newString += '1' * int.parse(ourString[i]);
        } else {
          newString += ourString[i];
        }
      }
      return newString.split('');
    }).toList();

    //print("The figure is:"+fieldList[moveToList[0]][moveToList[1]]);
    String valueToReturn = fieldList[moveToList[0]][moveToList[1]];
    return valueToReturn.toLowerCase() == 'p'
        ? ""
        : mapNameSymbol[valueToReturn];
  }

  int getCurrEmptyFieldsCount(String fen) {
    int currEmptyFieldsCount = 0;

    List fenElements = fen.split(" ")[0].split("/").join("").split("");
    fenElements.forEach((element) {
      if (isNumeric(element)) {
        currEmptyFieldsCount += int.parse(element);
      }
    });

    return currEmptyFieldsCount;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  String validateMovedFigure(currFen, moveFrom, moveTo) {
    return getMovedFigure(currFen.split(" ")[0], moveTo) == ""
        ? moveFrom.split("")[0]
        : getMovedFigure(currFen.split(" ")[0], moveTo);
  }

  List<Container> updateMovesText(moves) {
    List<Container> movesTextChildren = [];
    print(moves.split(" ").toString());
    for (var i = 1; i < moves.split(" ").length; i++) {
      var moveParts = moves.split(" ")[i].split(" ");
      movesTextChildren.add(Container(
        child: Text("  " + moveParts[0].toString() + " ",
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 20,
            )),
      ));
      //
      movesTextChildren.add(Container(
        decoration: BoxDecoration(
          border: _borders[int.parse(moveParts[0].split("")[0]) - 1][0],
          borderRadius: BorderRadius.circular(10),
          color: _colors[int.parse(moveParts[0].split("")[0]) - 1][0],
        ),
        child: InkWell(
            onLongPress: () {
              print(moveIsSelected);
              print(
                  "moveIndex: ${moveParts[0].split(".")[0]}, moveTurn: white");
            },
            onTap: () async {
              if (moveIsSelected &&
                  pervClickedSide == "w" &&
                  pervClickedIndex == int.parse(moveParts[0].split("")[0])) {
                moveIsSelected = false;
                setState(() {
                  _fen = steps.split("|")[steps.split("|").length - 2];

                  _colors.forEach((row) {
                    row[0] = Colors.transparent;
                    row[1] = Colors.transparent;
                  });

                  _borders.forEach((row) {
                    row[0] = Border.all(color: Colors.transparent);
                    row[1] = Border.all(color: Colors.transparent);
                  });

                  _textColors.forEach((row) {
                    row[0] = Colors.black;
                    row[1] = Colors.black;
                  });

                  this.movesTextChildren = updateMovesText(moves);
                });
              } else {
                //print("moveParts "+ moveParts.toString());
                int index = int.parse(moveParts[0].split(".")[0]);
                String currFen = steps.split("|")[index * 2 - 1];

                moveIsSelected = true;
                pervClickedIndex = index;
                pervClickedSide = 'w';
                //print('index: ' + index.toString());
                setState(() {
                  _fen = currFen;
                  indexToAttach = index;
                  _colors.forEach((row) {
                    row[0] = Colors.transparent;
                    row[1] = Colors.transparent;
                  });

                  _borders.forEach((row) {
                    row[0] = Border.all(color: Colors.transparent);
                    row[1] = Border.all(color: Colors.transparent);
                  });

                  _textColors.forEach((row) {
                    row[0] = Colors.black;
                    row[1] = Colors.black;
                  });

                  _borders[int.parse(moveParts[0].split('')[0]) - 1][0] =
                      Border.all(color: Colors.black);
                  _colors[int.parse(moveParts[0].split('')[0]) - 1][0] =
                      darkGreen;
                  _textColors[int.parse(moveParts[0].split('')[0]) - 1][0] =
                      Colors.white;
                });
                this.movesTextChildren = updateMovesText(moves);

                attachToIndex = index * 2 - 2;
                fenToStartVariation = steps.split("|")[index * 2 - 2];
              }
            },
            child: Text("  " + moveParts[1].toString() + " ",
                style: TextStyle(
                  color: _textColors[int.parse(moveParts[0].split("")[0]) - 1]
                      [0],
                  fontSize: 24,
                ))),
      ));

      if (moveParts.length == 3) {
        movesTextChildren.add(Container(
          decoration: BoxDecoration(
            border: _borders[int.parse(moveParts[0].split("")[0]) - 1][1],
            borderRadius: BorderRadius.circular(10),
            color: _colors[int.parse(moveParts[0].split("")[0]) - 1][1],
          ),
          child: InkWell(
              onLongPress: () {
                print(
                    "moveIndex: ${moveParts[0].split(".")[0]}, moveTurn: black");
              },
              onTap: () {
                if (moveIsSelected &&
                    pervClickedSide == "b" &&
                    pervClickedIndex == int.parse(moveParts[0].split("")[0])) {
                  moveIsSelected = false;
                  setState(() {
                    _fen = steps.split("|")[steps.split("|").length - 2];

                    _colors.forEach((row) {
                      row[0] = Colors.transparent;
                      row[1] = Colors.transparent;
                    });

                    _borders.forEach((row) {
                      row[0] = Border.all(color: Colors.transparent);
                      row[1] = Border.all(color: Colors.transparent);
                    });

                    _textColors.forEach((row) {
                      row[0] = Colors.black;
                      row[1] = Colors.black;
                    });

                    this.movesTextChildren = updateMovesText(moves);
                  });
                } else {
                  int index = int.parse(moveParts[0].split(".")[0]);
                  String currFen = steps.split("|")[index * 2];

                  pervClickedIndex = int.parse(moveParts[0].split("")[0]);
                  pervClickedSide = 'b';
                  moveIsSelected = true;
                  //print('index: ' + index.toString());
                  setState(() {
                    _fen = currFen;
                    indexToAttach = index;
                    _colors.forEach((row) {
                      row[0] = Colors.transparent;
                      row[1] = Colors.transparent;
                    });

                    _borders.forEach((row) {
                      row[0] = Border.all(color: Colors.transparent);
                      row[1] = Border.all(color: Colors.transparent);
                    });

                    _textColors.forEach((row) {
                      row[0] = Colors.black;
                      row[1] = Colors.black;
                    });

                    _borders[int.parse(moveParts[0].split('')[0]) - 1][1] =
                        Border.all(color: Colors.black);
                    _colors[int.parse(moveParts[0].split('')[0]) - 1][1] =
                        darkGreen;
                    _textColors[int.parse(moveParts[0].split('')[0]) - 1][1] =
                        Colors.white;
                  });
                  this.movesTextChildren = updateMovesText(moves);

                  attachToIndex = index * 2 - 1;
                  fenToStartVariation = steps.split("|")[index * 2 - 1];

                  //print(_colors);
                  //print("Ход " + moveParts[0].toString() + ", очередь черных");
                }
              },
              child: Text("  " + moveParts[2].toString() + " ",
                  style: TextStyle(
                    color: _textColors[int.parse(moveParts[0].split("")[0]) - 1]
                        [1],
                    fontSize: 24,
                  ))),
        ));
      }
      print(moves.split(" ")[i].split(" ").toString());
    }
    return movesTextChildren;
  }

  @override
  bool get wantKeepAlive => true;

  Future<void> showChooseSideScreen() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 30),
                    child: Text(
                      'Выберите сторону:',
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        child: SizedBox(
                            width: 45,
                            height: 45,
                            child: Image.asset('assets/images/pw.png')),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(Colors.white),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    side: BorderSide(color: Colors.black))),
                            fixedSize:
                                MaterialStateProperty.all<Size>(Size(70, 70))),
                        onPressed: () {
                          setState(() {
                            side = "white";
                            Navigator.of(context).pop();
                          });
                        },
                      ),
                      const SizedBox(
                        width: 30,
                        height: 10,
                      ),
                      ElevatedButton(
                        child: SizedBox(
                            width: 45,
                            height: 45,
                            child: Image.asset('assets/images/pb.png')),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(Colors.black),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                            fixedSize:
                                MaterialStateProperty.all<Size>(Size(70, 70))),
                        onPressed: () {
                          setState(() {
                            side = "black";
                            Navigator.of(context).pop();
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
