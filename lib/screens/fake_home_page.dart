import 'dart:ui';

import 'package:chess_app/ContentFillers/invariant_opening_content_fliier.dart';
import 'package:chess_app/Services/db.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stateless_chessboard/widgets/chessboard.dart';
import 'package:sortedmap/sortedmap.dart';
import 'package:sqflite/sqflite.dart';

import '../play_game_page.dart';
import '../utils.dart';

class FakeHomePage extends StatefulWidget {
  var moves;

  var blackOption =  false;

  FakeHomePage(this.moves, {this.blackOption = false});

  @override
  _FakeHomePageState createState() => _FakeHomePageState(moves, blackOption);
}

class _FakeHomePageState extends State<FakeHomePage> {
  final darkGreen = const Color.fromRGBO(116, 150, 90, 1);
  final lightGreen = const Color.fromRGBO(235, 239, 214, 1);
  final backColor = const Color.fromRGBO(32, 36, 33, 1);
  final gregColor = const Color.fromRGBO(237, 237, 237, 1);
  final changeColor = const Color.fromRGBO(226, 133, 24, 1);
  final deleteColor = const Color.fromRGBO(226, 24, 24, 1);

  int initialIndex = 1;
  int depth = 1;

  bool showLastMovePicker = false;
  var values = RangeValues(1, 1);
  var minLabel = 1;
  var maxLabel;

  Map movesAsMap = {};



  String side = 'white';
  var moves;
  bool showTDialogSide = true;
  final String _fakeFen =
      'RNBKQBNR/PPPPPPPP/8/8/8/8/pppppppp/rnbkqbnr b KQkq - 0 1';

  _FakeHomePageState(this.moves, this.blackOption);

  var blackOption =  false;

  int currmMaxIndex = 1;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    var AlertDialogSide = AlertDialog(
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 150,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.only(bottom: 30),
                child: Text(
                  'Выберите сторону:',
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    child: SizedBox(
                        width: 45,
                        height: 45,
                        child: Image.asset('assets/images/pw.png')),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                        shape:
                        MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.black))),
                        fixedSize:
                        MaterialStateProperty.all<Size>(Size(70, 70))),
                    onPressed: () {
                      moves.forEach((key, value) => movesAsMap[key] = value);
                      side = "white";
                      int movementsCount =
                          movesAsMap['movements'].split('|').length - 1;
                      movementsCount -= movementsCount % 2;
                      movementsCount = (movementsCount / 2).round();
                      depth = movementsCount;
                      currmMaxIndex = movementsCount;



                      setState(() {
                        showTDialogSide = false;
                      });
                    },
                  ),
                  const SizedBox(
                    width: 30,
                    height: 10,
                  ),
                  ElevatedButton(
                    child: SizedBox(
                        width: 45,
                        height: 45,
                        child: Image.asset('assets/images/pb.png')),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                        shape:
                        MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                        fixedSize:
                        MaterialStateProperty.all<Size>(Size(70, 70))),
                    onPressed: () {
                      moves.forEach((key, value) => movesAsMap[key] = value);

                      side = "black";
                      int movementsCount =
                          movesAsMap['movements'].split('|').length - 1;
                      movementsCount -= movementsCount % 2;
                      movementsCount = (movementsCount / 2).round();
                      depth = movementsCount;
                      currmMaxIndex = movementsCount;

                      setState(() {
                        showTDialogSide = false;
                      });
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
    RangeValues _currentRangeValues = RangeValues(1, depth.toDouble());
    var maxIndex1 = depth;
    var AlertDialogMove = AlertDialog(
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom:25.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Text("Начать с хода: " + initialIndex.toString() ,style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),),
                      ],
                    ),
                  ],
                ),
              ),

              Row(
                children: [
                  Text("1"),
                  Slider(
                    activeColor: darkGreen,
                    inactiveColor: lightGreen,
                    thumbColor: darkGreen,
                    value: double.parse(initialIndex.toString()) ,
                    max: double.parse(depth.toString()),
                    min:  1,
                    label: initialIndex.toString(),
                    onChanged: (double value) {
                      setState(() {
                        initialIndex = value.round();
                      });
                    },
                  ),
                  Text(depth.toString())
                ],
              ),
              Row(
                children: [
                  Text("Задать глубину хода"),
                  Switch(
                    value: showLastMovePicker, onChanged: (bool value) {
                      setState(() {
                        showLastMovePicker = !showLastMovePicker;
                        depth = currmMaxIndex;
                      });
                    },
                    activeColor: darkGreen,
                  )
                ]
              ),
              showLastMovePicker ?
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: (){
                      print("maxIndex >= 1 ${depth >= 1}");
                      if(depth+1 <= currmMaxIndex){
                        setState(() {
                          print(currmMaxIndex);
                          depth ++;
                        });
                      }
                    },
                    child: Icon(Icons.exposure_plus_1, color: Colors.black),
                    style: TextButton.styleFrom(
                      backgroundColor: lightGreen,
                    ),
                  ),
                  SizedBox(width: 15,),
                  Text("${depth-initialIndex+1}"),
                  SizedBox(width: 15,),
                  TextButton(
                      onPressed: (){
                        if(depth-1 >= 1){{
                          setState(() {
                            depth --;
                          });
                        }}
                      },
                      child: Icon(Icons.exposure_minus_1, color: Colors.black,),
                      style: TextButton.styleFrom(
                          backgroundColor: lightGreen,
                      ),
                  ),
                ],
              ) : Row(children: [],),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    child: Text(
                      "Смотреть",
                      style: TextStyle(color: lightGreen),
                    ),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueGrey),
                        shape:
                        MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.black))),
                        fixedSize:
                        MaterialStateProperty.all<Size>(Size(100, 45))
                    ),
                    onPressed: () {
                      print((initialIndex-1)*2);
                      !blackOption ?
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PlayGamePage(movesAsMap, side,depth, true,
                                initialIndex: (initialIndex-1)*2)
                        ),
                      ):
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PlayGamePage(
                                    movesAsMap,
                                    side,depth, true,
                                    initialIndex: (initialIndex-1)*2 - (side == 'white' ? -1 : 1)
                                )
                        ),
                      );
                    },
                  ),
                  SizedBox(width: 20,),
                  ElevatedButton(
                    child: Text(
                      "К игре!",
                      style: TextStyle(color: lightGreen),
                    ),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(darkGreen),
                        shape:
                        MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.black))),
                        fixedSize:
                        MaterialStateProperty.all<Size>(Size(85, 45))
                    ),
                    onPressed: () {

                      moves.forEach((key, value) => movesAsMap[key] = value);
                      side = movesAsMap["side"];

                      blackOption = side == "black";

                      print(movesAsMap);
                      print((initialIndex-1)*2);
                      !blackOption ?
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PlayGamePage(movesAsMap, side,depth, false,
                                initialIndex: (initialIndex-1)*2)
                        ),
                      ):
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PlayGamePage(
                                    movesAsMap,
                                    side,depth,false,
                                    initialIndex: (initialIndex-1)*2 - (side == 'white' ? -2 : 0)
                                )
                        ),
                      );
                    },
                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Scaffold(
          appBar: AppBar(
            title: Text("name"),
            backgroundColor: backColor,
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Chessboard(
                  darkSquareColor: Color.fromRGBO(116, 150, 90, 1),
                  lightSquareColor: Color.fromRGBO(235, 239, 214, 1),
                  fen: _fakeFen,
                  size: size.width,
                  onMove: (move) {},
                ),
              ],
            ),
          ),
        ),
        Positioned.fill(
          child: Center(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
              child: AlertDialogMove,
            ),
          ),
        ),
      ],
    );
  }

}
