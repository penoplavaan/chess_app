import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Services/db.dart';
import 'package:sqflite/sqflite.dart';

class UserContentFiller {

  static const String dbName = 'openings';
  static const String tableName = 'user';
  static const String onCreateString =
      //'DROP TABLE  IF EXISTS $tableName';
  'CREATE TABLE IF NOT EXISTS user (id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, first_time_opened INTEGER, externalId INTEGER)';
}
