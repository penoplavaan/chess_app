import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Services/db.dart';
import 'package:sqflite/sqflite.dart';

class VariantOfOpeningContentFiller {

  static const String dbName = 'openings';
  static const String tableName = 'variants_of_openings';
  static const String onCreateString =
      //'DROP TABLE  IF EXISTS $tableName';
  'CREATE TABLE  IF NOT EXISTS $tableName (id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, attachToIndex INTEGER, movements STRING, movementsTextual STRING, importance INTEGER)';
}
