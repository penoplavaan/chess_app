import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Services/db.dart';
import 'package:sqflite/sqflite.dart';

class CommentaryContentFiller {

  static const String dbName = 'openings';
  static const String tableName = 'commentaries';
  static const String onCreateString =
      //'DROP TABLE  IF EXISTS $tableName';
  'CREATE TABLE  IF NOT EXISTS $tableName (id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, openingID INTEGER, attachToIndex INTEGER, side STRING, commentaryText STRING)';
}
