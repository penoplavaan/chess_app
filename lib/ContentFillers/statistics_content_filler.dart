import 'package:chess_app/Models/opening_no_variant.dart';
import 'package:chess_app/Services/db.dart';
import 'package:sqflite/sqflite.dart';

class StatisticsContentFiller {

  static const String dbName = 'openings';
  static const String tableName = 'statistics';
  static const String onCreateString =
      //'DROP TABLE  IF EXISTS $tableName';
  'CREATE TABLE IF NOT EXISTS statistics (id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,openingId INTEGER,  total_play INTEGER, mistakes INTEGER, wins INTEGER, loses INTEGER,FOREIGN KEY(openingId) REFERENCES invariant_openings(id) ON DELETE CASCADE)';
}
