class InvariantOpeningContentFiller {


  static const String dbName = 'openings';
  static const String tableName = 'invariant_openings';
  static const String onCreateString =
  //'DROP TABLE  IF EXISTS $tableName';
  'CREATE TABLE  IF NOT EXISTS $tableName (id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL , name STRING, subName STRING, movements STRING, movementsTextual STRING, attachedVariants STRING, side STRING)';
}
