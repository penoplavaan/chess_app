import 'package:chess_app/screens/intro/intro_screen.dart';
import 'package:chess_app/screens/main_screen/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

import 'ContentFillers/user_content_filler.dart';
import 'Models/user.dart';
import 'Services/db.dart';
import 'play_game_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: checkIfFirstTime(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
          print('snapshot: $snapshot');
        if (snapshot.hasData){
          var data = snapshot.data[0];
          print(data);
          switch (data['first_time_opened']){
            case 1:
              return MaterialApp(
                title: 'Welcome to Flutter',
                home: IntroScreen(),
              );
            case 0:
              return MaterialApp(
                title: 'Welcome to Flutter',
                home: DismissibleApp(),
              );
          }
        }
        return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      }
    );
  }


  Future checkIfFirstTime() async {
    Database db = await openDatabase(
        await getDatabasesPath() + UserContentFiller.dbName);
    await db.execute(UserContentFiller.onCreateString);
    DB dbUserManipulator = DB(
        db,
        UserContentFiller.dbName,
        UserContentFiller.tableName,
        UserContentFiller.onCreateString);

    dbUserManipulator.insert(User(id: 1, first_time_opened: 1));
    var user = dbUserManipulator.query(1);
    return user;
  }

  Future markAsOpened() async {
    Database db = await openDatabase(
        await getDatabasesPath() + UserContentFiller.dbName);
    await db.execute(UserContentFiller.onCreateString);
    DB dbUserManipulator = DB(
        db,
        UserContentFiller.dbName,
        UserContentFiller.tableName,
        UserContentFiller.onCreateString);

    User newUser = User(id: 1, first_time_opened: 0);
    dbUserManipulator.update(newUser);
  }
}