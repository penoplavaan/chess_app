import 'package:chess_app/ContentFillers/user_content_filler.dart';
import 'package:chess_app/Models/model.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:sqflite/sqflite.dart';

class DB {
  Database _db;
  String _dbName;
  String _tableName;
  String _onCreateString;

  DB(this._db, this._dbName, this._tableName, this._onCreateString);

  Future<Database> init() async {
    String _path = await getDatabasesPath() + _dbName;
    return await openDatabase(_path, onCreate: onCreate);
  }

  void onCreate(Database _db, int version) async =>
      await _db.execute(_onCreateString);

  Future<List<Map<String, dynamic>>> query(int id) async =>
      await _db.query(_tableName, where: 'id=?', whereArgs: [id]);

  Future<List<Map<String, dynamic>>> queryAll() async =>
      await _db.query(_tableName);

  Future<List<Map<String, dynamic>>> queryAllBySideOffline(String side) async =>
      await _db.query(_tableName, where: 'side=?', whereArgs: [side]);

  Future<Response> queryAllBySide(String side) async {
    Database db =
        await openDatabase(await getDatabasesPath() + UserContentFiller.dbName);
    await db.execute(UserContentFiller.onCreateString);
    DB dbUserManipulator = DB(db, UserContentFiller.dbName,
        UserContentFiller.tableName, UserContentFiller.onCreateString);

    String userId =
        (await dbUserManipulator.query(1))[0]['externalId'].toString();

    return http.get(Uri.http('penoplavaan.space:3000', '/openings',
        {'side': side, 'userid': userId}));
  }

  // await _db.rawQuery(
  //     'SELECT * FROM $_tableName WHERE side=?',
  //     [side]
  // );

  Future<List<Map<String, dynamic>>> queryCommentary(
          int openingID, int attachToIndex, String side) async =>
      await _db.rawQuery(
          'SELECT * FROM $_tableName WHERE openingID=? and attachToIndex=? and side=?',
          [openingID, attachToIndex, side]);

  Future<int> insert(Model model) async {
    try {
      await _db.insert(_tableName, model.toMap());
    } catch (DatabaseException) {
      print('Попытка добавить неуникальное значение!');
    }
    return 0;
  }

  Future<int> update(Model model) async =>
      await _db.update(_tableName, model.toMap(),
          where: 'id = ?', whereArgs: [model.id]);

  Future<int> delete(int id) async =>
      await _db.delete(_tableName, where: 'id = ?', whereArgs: [id]);
}
